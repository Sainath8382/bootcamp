
#include<stdio.h>
#include<stdlib.h>

struct Node{
	
	int data;
	struct Node *prev;
	struct Node *next;
};

struct Node *head=NULL;

struct Node* createNode(){

	struct Node *newNode = (struct Node*)malloc(sizeof(struct Node));

	newNode->prev = NULL;
	
	printf("Enter data\n");
	scanf("%d",&newNode->data);

	newNode->next=NULL;

	return newNode;
}

void addNode(){

	struct Node* newNode = createNode();

	if(head==NULL){
		head=newNode;
	}else{
		
		struct Node *temp = head;

		while(temp->next!= NULL){
			temp = temp->next;
		}
		
		newNode->prev = temp;
		temp->next =newNode;
	}

}


void addFirst(){
	
	struct Node *newNode = createNode();

	if(head==NULL){
		head = newNode;
	}else{
		newNode->next = head;
		head->prev = newNode;
		head = newNode;
	}

}

int countNode(){
	if(head==NULL){
		printf("The Doubly LL is empty\n");
		return -1;
	}
	int count = 0;
	struct Node *temp=head;

	while(temp!=NULL){
		count++;
		temp = temp->next;
	}

	return count;
}

int addatPos(int pos){

	int count = countNode();

	if(pos<=0 || pos>= count+2){
		printf("Invalid position\n");
		return -1;
	}else{
		if(pos ==1){
			addFirst();
		}else if(pos == count+1){
			addNode();
		}else{
			struct Node *newNode = createNode();
			struct Node *temp=head;

			while(pos-2){
				temp = temp->next;
				pos--;
			}
			newNode->next = temp->next;
			newNode->prev = temp;
			temp->next->prev = newNode;
			temp->next = newNode;
		}return 0;
			
	}
	
}

int delFirst(){
	
	if(head== NULL){
		printf("The linked list is empty\n");
		return -1;
	}else{
		struct Node *temp= head;
		head = head->next;
		head->prev = NULL;

		free(temp);
		temp=NULL;
	}
}

int delLast(){
	
	if(head==NULL){
		printf("The LL is empty\n");
		return -1;
	}else{
		
		struct Node *temp=head;
		
		while(temp->next->next != NULL)
			temp=temp->next;
		
		free(temp->next);
		temp->next = NULL;
	}	
	return 0;
}

int DelatPos(int pos){

	int count = countNode();

	if(head==NULL){
		printf("The LL is empty\n");
		return -1;
	}

	if(pos<=0 || pos>count){
		printf("Invalid position\n");
		return -1;
	}else{
		if(pos==1){
			delFirst();
		}else if(pos==count){
			delLast();
		}else{

			struct Node *temp = head;
			while(pos-2){
				temp=temp->next;
				pos--;
			}
			struct Node *temp2= temp->next;
			temp->next = temp2->next;
			temp->next->prev = temp;
			free(temp2);
			temp2 = NULL;
		}
		return 0;
	}
}


int printLL(){
	
	if(head == NULL){
		printf("The Linked List is empty\n");
		return -1;
	}else{
	struct Node *temp=head;

	while(temp->next!=NULL){

		printf("|%d|->",temp->data);
		temp=temp->next;
	}
	printf("|%d|\n",temp->data);
	}
}

void main(){

	char choice;

	do{
		
		printf("1.addNode\n");
		printf("2.addFirst\n");
		printf("3.addatPos\n");
		printf("4.delFirst\n");
		printf("5.delatPos\n");
		printf("6.delLast\n");
		printf("7.PrintLL\n");
		printf("8.Add Multiple Nodes\n");

		int ch;
		printf("Enter choice:");
		scanf("%d",&ch);

		switch(ch){

			case 1:
				addNode();
				printLL();
				break;
				
			case 2:
				addFirst();
				printLL();
				break;

			case 3:{
				int pos;
				printf("Enter pos:");
				scanf("%d",&pos);

				addatPos(pos);
				  
				break;
			       }

			case 4:
				delFirst();
				printLL();
				break;

			case 5:{
				int pos1;
				printf("Enter pos:\n");
				scanf("%d",&pos1);

				DelatPos(pos1);
				printLL();
			       }
				break;

			case 6: 
				delLast();
				printLL();
				break;
			
			case 7:
				printLL();
				break;

			case 8:
		{
				int nodes;
				printf("Enter the count of nodes to be added at once\n");
				scanf("%d",&nodes);

				for(int i=0;i<nodes;i++){
					addNode();
				}
				break;
				}

			default:
				printf("Wrong choice");
				break;
		}
		getchar();
		
		printf("COntinue ?\n");
		scanf("%c",&choice);

	}while(choice=='Y' || choice=='y');
}












			

	













































