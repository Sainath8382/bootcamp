
#include<stdio.h>
#include<stdlib.h>

struct Node{

	int data;
	struct Node *next;
};

struct Node *head=NULL;

struct Node* createNode(){

	struct Node *newNode = (struct Node*)malloc(sizeof(struct Node));

	printf("Enter data:");
	scanf("%d",&newNode->data);
	newNode->next=NULL;

	return newNode;
}

void addNode(){

	struct Node *newNode=createNode();

	if(head==NULL){
		head=newNode;
	}else{
		struct Node *temp=head;

		while(temp->next!=NULL)
			temp=temp->next;

		temp->next=newNode;
	}
}


void addFirst(){

	struct Node *newNode=createNode();
	
	if(head==NULL)
		head=newNode;
	else{
		newNode->next=head;
		head=newNode;
	}
}

int countNode(){
	
	if(head==NULL){
		return 0;
	}else{
	int count =0;

	struct Node *temp=head;

	while(temp!=NULL){
		count++;
		temp=temp->next;
	}

	return count;
	}

}

int addatPos(int pos){
	
	int count=countNode();
	
	if(pos<=0 || pos>count+1){
		return -1;
	}else{
	if(pos==1)
		addFirst();
	else if(pos==count+1)
		addNode();
	else{
		struct Node *newNode=createNode();
		struct Node *temp = head;

		while(pos-2){
			temp=temp->next;
			pos--;
		}
	
		newNode->next=temp->next;
		temp->next=newNode;
		}
	}
	return 0;
}

int delFirst(){
	if(head==NULL){
		printf("Invalid Operation\n");
		return -1;	
	}/*else if(head->next==NULL){
		free(head);
		head=NULL;
	}*/else{
	struct Node *temp = head;
	head=head->next;

	free(temp);
	}
	return 0;
}

int delLast(){

	if(head==NULL){
		printf("LL is empty\n");
		return -1;
	}else{
		
		struct Node *temp=head;
		while(temp->next->next!=NULL){
			temp=temp->next;
		}
	
		free(temp->next);
		temp->next=NULL;
	}
	return 0;	
}

int DelatPos(int pos){

	int count=countNode();

	if(head==NULL){
		printf("Invalid operation\n");
		return -1;
	}	

		if(pos<=0 || pos>count){
			printf("Invalid position\n");
			return -2;
		}else{
        		if(pos==1){
				delFirst();
			}else if(pos==count){
				delLast();
			}else{
				struct Node *temp=head;

				while(pos-2){
					temp=temp->next;
					pos--;
				}

				struct Node *temp2=temp->next;
				temp->next=temp2->next;
				free(temp2);
				//temp2=NULL;
			}
	
		}
	
}


int printLL(){
	
	if(head == NULL){
		printf("The Linked List is empty\n");
		return -1;
	}else{
	struct Node *temp=head;

	while(temp->next!=NULL){

		printf("|%d|",temp->data);
		temp=temp->next;
	}
	printf("|%d|\n",temp->data);
	}
}

void main(){

	char choice;

	do{
		
		printf("1.addNode\n");
		printf("2.addFirst\n");
		printf("3.addatPos\n");
		printf("4.delFirst\n");
		printf("5.delatPos\n");
		printf("6.delLast\n");
		printf("7.PrintLL\n");

		int ch;
		printf("Enter choice:");
		scanf("%d",&ch);

		switch(ch){

			case 1:
				addNode();
				printLL();
				break;
				
			case 2:
				addFirst();
				printLL();
				break;

			case 3:
				int pos;
				printf("Enter pos:\n");
				
				scanf("%d",&pos);
				addatPos(pos);
				printLL();
				break;

			case 4:
				delFirst();
				printLL();
				break;

			case 5:
				int pos1;
				printf("Enter pos:\n");
				scanf("%d",&pos1);

				DelatPos(pos1);
				printLL();
				break;

			case 6: 
				delLast();
				printLL();
				break;
			
			case 7:
				printLL();
				break;

			default:
				printf("Wrong choice");
				break;
		}
		getchar();
		
		printf("COntinue ?\n");
		scanf("%c",&choice);

	}while(choice=='Y' || choice=='y');
}












			

	













































