//concat both 2 Singly LL's

#include<stdio.h>
#include<stdlib.h>

typedef struct Node{
	int data;
	struct Node *next;
}Node;

Node *createNode(){
	
	Node *newNode = (Node*)malloc(sizeof(Node));
	printf("Enter data: ");
	scanf("%d",&newNode->data);

	newNode->next = NULL;

	return newNode;
}

Node *head1 =NULL;
Node *head2 =NULL;

void addNode(Node* head){

	Node *newNode = createNode();

	if(head == NULL)
		head=newNode;
	else{
		Node *temp = head;
		while(temp->next != NULL)
			temp = temp->next;

		temp->next = newNode;
	}
}

int countNode(Node* head){
	int count =0;

	Node *temp=head;
       while(temp!=NULL){
		count++;
 		temp=temp->next;
       }
	return count;       
}

int printLL(Node* head){

/*	if(head==NULL){
		printf("LL is empty\n");
		return -1;
	}else{*/
		Node *temp = head;
		while(temp->next != NULL){
			printf("|%d|->",temp->data);
			temp=temp->next;
		}
		printf("|%d|",temp->data);
	
	return 0;
}


void concatN(int num){

	int val = countNode() - num;
	Node *temp1=head1;
	Node *temp2= head2;

	while(temp1->next!=NULL)
		temp1=temp1->next;

	while(val){
		temp2= temp2->next;
		val--;
	}


	temp1->next=temp2;
}
void main(){

	int nodes,nodes2;
	printf("Enter nodes for 1st LL : ");
	scanf("%d",&nodes);

	for(int i=1;i<=nodes;i++){
		addNode(head1);
	}
	printf("LL1 is : ");
	printLL(head1);

	printf("Enter nodes for LL2 : ");
	scanf("%d",&nodes2);

	for(int i=1;i<=nodes2;i++)
		addNode(head2);

	printf("LL2 is : ");
	printLL(head2);

	int num;

	printf("Enter number of nodes to concat\n");
	scanf("%d",&num);

	concatN(num);

	printf("After concating 2 LL's \n");
	printLL(head1);
}



















