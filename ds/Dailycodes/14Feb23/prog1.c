//Quick sort

#include<stdio.h>

void swap(int *no1,int *no2){
	int temp=*no1;
	*no1=*no2;
	*no2=temp;
}

int partition(int *arr,int start,int end){
	int pivot=arr[end];
	int itr=start-1;

	for(int i=start;i<end;i++){
		if(arr[i]<pivot){
			itr++;
			swap(&arr[i],&arr[itr]);
		}
	}

	swap(&arr[itr+1],&arr[end]);
	return (itr+1);
}

void quickSort(int *arr,int start,int end){
	if(start<end){
		int pivot=partition(arr,start,end);
		quickSort(arr,start,pivot-1);
		quickSort(arr,pivot+1,end);
	}
}

void main(){
	int arr[]={-4,8,-2,1,7,3};
	int size=6;
	int start=0,end=size-1;

	quickSort(arr,start,end);

	printf("The sorted array is \n");
	for(int i=0;i<size;i++){
		printf("%d\t",arr[i]);
	}
	printf("\n");
}
