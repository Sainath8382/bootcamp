//Singly circular linkded list

#include<stdio.h>
#include<stdlib.h>

struct Node{
    int data;
    struct Node *next;
};

struct Node *createNode(){
    struct Node *newNode=(struct Node*)malloc(sizeof(struct Node));
    printf("Enter data:\n");
    scanf("%d",&newNode->data);
    newNode->next=NULL;

    return newNode;
}

struct Node *head=NULL;

void addNode(){
    struct Node *newNode=createNode();
    if(head==NULL){
        head=newNode;
        newNode->next=head;
    }else{
        struct Node *temp=head;
        while(temp->next!=head){
            temp=temp->next;
        }

        temp->next=newNode;
        newNode->next=head;
    }
}

void addFirst(){
    if(head==NULL){
        addNode();
    }else{
        struct Node *newNode=createNode();
        struct Node *temp=head;
        while(temp->next!=head)
            temp=temp->next;

        newNode->next=head;
        temp->next=newNode;
        head=newNode;
    }
}

void addLast(){
    addNode();
}

int nodecount(){
    struct Node *temp=head;
    int count=0;
    while(temp->next!=temp){
        count++;
        temp=temp->next;
    }
    printf("count = %d\n",count);
    return count;
}

int addatPos(int pos){
    int count=nodecount();
    if(pos<=0 || pos >count+2){
        printf("Invalid node position\n");
        return -1;
    }else{
        if(pos==1){
            addFirst();
        }else if(pos==count+1){
            addLast();
        }else{
            struct Node *newNode=createNode();
            struct Node *temp=head;
            while(pos-2){
                temp=temp->next;
                pos--;
            }
            newNode->next=temp->next;
            temp->next=newNode;
        }
        return 0;
    }
}

void delFirst(){
    //int count=nodecount();
    if(head==NULL){
        printf("Linked List empty\n");
//        return -1;
    }else{
        if(head->next==head){
            free(head);
            head=NULL;
        }else{
            struct Node *temp=head;
            head=temp->next;
            free(temp);

            struct Node *temp2=head;
            while(temp2->next!=temp){
                temp2=temp2->next;
            }
            temp2->next=head;
        }
        //return 0;
    }
}

int delLast(){
    //int count=nodecount();
    if(head==NULL)
    {
        printf("LL empty\n");
        return -1;
    }else{
        if(head->next==head){
            free(head);
            head=NULL;
        }else{
            struct Node *temp=head;
            while(temp->next->next!=head)
                temp=temp->next;

            free(temp->next);
            temp->next=head;
        }
        return 0;
    }
}

void delatPos(int pos){
    int count=nodecount();
    if(pos<=0 || pos>count){
        printf("Invalid operation\n");
        //return -1;
    }else{
        if(pos==1){
            delFirst();
        }else if(pos==count){
            delLast();
        }else{
        struct Node *temp=head;
            while(pos-2){
                temp=temp->next;
                pos--;
            }
            struct Node *temp2=temp->next;
            temp->next=temp->next->next;
            free(temp2);
        }
       // return 0;
    }
}
/*
int deleteAtPos(int pos){
	int count = countNode();
	if(pos<=0 || pos>count){
		printf("INVLID POSITION\n");
		return -1;
	}else{
		if(pos == 1)
			deleteFirst();
		else if(pos == count)
			deleteLast();
		else{
			struct Node *temp = head;
			while(pos-2){
				temp = temp->next;
				pos--;
			}
			struct Node *ptr = temp->next;
			temp->next = temp->next->next;
			free(ptr);
		}
		return 0;
	}
}
*/
void printLL(){
    if(head==NULL){
        printf("LL empty\n");
        return -1;
    }else{
        struct Node *temp=head;
        while(temp->next!=head){
            printf("|%d|->",temp->data);
            temp=temp->next;
        }
        printf("|%d|\n",temp->data);
        return 0;
    }
}

void main(){
    char choice;

    do{
        printf("1.addNode\n");
        printf("2.addFirst\n");
        printf("3.Addlast\n");
        printf("4.AddatPos\n");
        printf("5.deleteFirst\n");
        printf("6.deleteLast\n");
        printf("7.deleteatPos\n");
        printf("8.nodecount\n");
        printf("9.printLL\n");

        int ch;
        printf("Enter your choice:\n");
        scanf("%d",&ch);
        switch(ch){
                        case 1:
                addNode();
                break;

            case 2:
                addFirst();
                break;

            case 3:
                addLast();
                break;

            case 4:{
                int pos;
                printf("Enter the pos:\n");
                scanf("%d",&pos);

                addatPos(pos);
                }
                break;

             case 5:
                delFirst();
                break;

             case 6:
                delLast();
                break;

             case 7:{
                int pos;
                printf("Enter the pos:\n");
                scanf("%d",&pos);

                delatPos(pos);
                }
                break;

            case 8:
                nodecount();
                break;

            case 9:
                printLL();
                break;

            default:
                printf("Wrong choice\n");
            }
        getchar();
        printf("Do you want to continue:\n");
        scanf("%c",&choice);
    }while(choice=='Y' || choice=='y');
}
