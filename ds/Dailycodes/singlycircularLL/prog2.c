//singly circular ll

#include<stdio.h>
#include<stdlib.h>
struct Node{
    int data;
    struct Node *next;
};

struct Node *createNode(){
        struct Node *newNode=(struct Node*)malloc(sizeof(struct Node));
        printf("Enter data: ");
        scanf("%d",&newNode->data);
        newNode->next=NULL;

        return newNode;
}

struct Node *head=NULL;

void addNode(){
    struct Node *newNode=createNode();
    if(head==NULL){
        head=newNode;
        newNode->next=head;
    }else{
        struct Node *temp=head;
        while(temp->next!=head)
            temp=temp->next;

        newNode->next=head;
        temp->next=newNode;
    }
}

int addFirst(){
    struct Node *newNode=createNode();
    if(head==NULL){
        head=newNode;
        newNode->next=head;
    }else{
        struct Node *temp=head;
        while(temp->next!=head){
            temp=temp->next;
        }
        newNode->next=head;
        temp->next=newNode;
        head=newNode;
    }
}

int nodecount(){
    int count=0;
    if(head==NULL){
        printf("LL empty\n");
    }else{
    struct Node *temp=head;
    while(temp->next!=temp){

        temp=temp->next;
        count++;
    }
    printf("Count=%d\n",count);
    return count;
    }
}

void addLast(){
    addNode();
}

int addatPos(int pos){
    int count=nodecount();
    if(pos<=0 || pos>=count+2){
        printf("Invalid operation\n");
        return -1;
    }else{
        if(pos==1){
            addFirst();
        }else if(pos==count+1){
            addLast();
        }else{
            struct Node *newNode=createNode();
            struct Node *temp=head;
            while(pos-2){
                temp=temp->next;
                pos--;
            }
            newNode->next=temp->next;
            temp->next=newNode;
        }
        return 0;
    }
}

int delFirst(){
    if(head==NULL){
        printf("LL empty\n");
        return -1;
    }else{
        if(head->next==head){
            free(head);
            head=NULL;
        }else{
            struct Node *temp=head;
            while(temp->next!=head){
                temp=temp->next;
            }
            head=head->next;
            free(temp->next);
            temp->next=head;
        }
        return 0;
    }
}

int delLast(){
    if(head==NULL){
        printf("LL empty\n");
        return -1;
    }else{
        if(head->next=head){
            free(head);
            head=NULL;
        }else{
            struct Node *temp=head;
            while(temp->next->next!=NULL){
                temp=temp->next;
            }
            free(temp->next);
            temp->next=head;
        }
        return 0;
    }
}

int delatPos(int pos){
    int count=nodecount();
    if(pos<=0 || pos>count){
        printf("Invalid operation\n");
        return -1;
    }else{
        if(pos==1){
            delFirst();
        }else if(pos==count){
            delLast();
        }else{
            struct Node *temp=head;
            while(pos-2){
                temp=temp->next;
                pos--;
            }
            struct Node *temp2=head;
            temp2=temp->next;
            temp->next=temp2->next;
            free(temp2);
        }
        return 0;
    }
}

void printLL(){
    struct Node *temp=head;
    if(head==NULL){
            printf("LL empty\n");
    }else{
    while(temp->next!=head){
        printf("| %d |->",temp->data);
        temp=temp->next;
    }
    printf("| %d |",temp->data);
    }
}

void main(){
    char choice;

    do{
        printf("Welcome to singly circular Linked List !!!\n");
        int ch;
        printf("1.addNode\n");
        printf("2.addFirst\n");
        printf("3.addatPos\n");
        printf("4.addLast\n");
        printf("5.delFirst\n");
        printf("6.delLast\n");
        printf("7.delatPos\n");
        printf("8.count\n");
        printf("9.Printll\n");
        printf("Enter your choice: ");
        scanf("%d",&ch);

        switch(ch){
        case 1:
        addNode();
        break;

        case 2:
        addFirst();
        break;

        case 3:{
        int pos;
        printf("Enter the position:\n");
        scanf("%d",&pos);
        addatPos(pos);
        }
        break;

        case 4:
        addLast();
        break;

        case 5:
        delFirst();
        break;

        case 6:
        delLast();
        break;

        case 7:{
        int pos;
        printf("Enter the position:\n");
        scanf("%d",&pos);

        delatPos(pos);
        }
        break;

        case 8:
            nodecount();
            break;

        case 9:
            printLL();
            break;

        default:
        printf("Wrong choice\n");
        break;
        }
        getchar();
        printf("Do you want to continue y\n: ");
        scanf("%c",&choice);
    }while(choice=='Y'|| choice=='y');
}
