//singly cirular linked list is a type of singly linked list which has a loop in it
//that means last node of the linked list is pointing towards first node
//that is last nodes next = first nodes address

#include<stdio.h>
#include<stdlib.h>

struct node{
	int data;
	struct node *next;
};

struct node *head;

struct node* createNode(){
	struct node *newNode = (struct node*)malloc(sizeof(struct node));
	printf("Enter data : ");
	scanf("%d",&newNode->data);
	newNode->next = NULL;
}

void addNode(){
	struct node *newNode = createNode();
	if(head == NULL){
		head = newNode;
		newNode->next = head;
	}else{
		struct node *temp = head;
		while(temp->next != head)
			temp = temp->next;
		temp->next = newNode;
		newNode->next = head;
	}
}

void addFirst(){
	struct node *newNode = createNode();
	if(head == NULL){
		head = newNode;
		newNode->next = head;
	}else{
		struct node *temp = head;
		newNode->next = head;
		while(temp->next != head)
			temp = temp->next;
		temp->next = newNode;
		head = newNode;
	}
}

int countNode(){
	if(head == NULL){
		return 0;
	}else if(head->next == head){
		return 1;
	}else{
		int count = 0;
		struct node *temp = head;
		while(temp->next != head){
			count++;
			temp = temp->next;
		}
		return count;
	}
}

int addAtPos(int pos){
	int count = countNode();
	if(pos<=0 || pos>count+1){
		printf("INVALID POSITION\n");
		return -1;
	}else{
		if(pos == 1)
			addFirst();
		else if(pos == count+1)
			addNode();
		else{
			struct node *newNode = createNode();
			struct node *temp = head;
			while(pos-2){
				temp = temp->next;
				pos--;
			}
			newNode->next = temp->next;
			temp->next = newNode;
		}
		return 0;
	}
}

void addLast(){
	addNode();
}

int deleteFirst(){
	if(head == NULL){
		printf("EMPTY LINKED LIST\n");
		return -1;
	}else{
		if(head->next == head){
			free(head);
			head = NULL;
		}else{
			struct node *temp1 = head;
			head = temp1->next;
			free(temp1);
			struct node *temp2 = head;
			while(temp2->next != temp1)
				temp2 = temp2->next;
			temp2->next = head;
		}
		return 0;
	}
}

int deleteLast(){
	if(head == NULL){
		printf("EMPTY LINKED LIST\n");
		return -1;
	}else{
		if(head->next == head){
			free(head);
			head = NULL;
		}else{
			struct node *temp = head;
			while(temp->next->next != head)
				temp = temp->next;
			free(temp->next);
			temp->next = head;
		}
		return 0;
	}
}

int deleteAtPos(int pos){
	int count = countNode();
	if(pos<=0 || pos>count){
		printf("INVLID POSITION\n");
		return -1;
	}else{
		if(pos == 1)
			deleteFirst();
		else if(pos == count)
			deleteLast();
		else{
			struct node *temp = head;
			while(pos-2){
				temp = temp->next;
				pos--;
			}
			struct node *ptr = temp->next;
			temp->next = temp->next->next;
			free(ptr);
		}
		return 0;
	}
}


int display(){
	if(head == NULL){
		printf("EMPTY LINKED LIST\n");
		return -1;
	}else{
		struct node *temp = head;
		while(temp->next != head){
			printf("|%d|->",temp->data);
			temp = temp->next;
		}printf("|%d|\n",temp->data);
		return 0;
	}
}



void main(){
	printf("WELCOME TO SINGLY CIRCULAR MENU\n");
	char choice;
        do{
                printf("--------------------\n");
                printf("| 1.addnode        |\n");
                printf("| 2.addFirst       |\n");
                printf("| 3.addLast        |\n");
                printf("| 4.addpos         |\n");
                printf("| 5.display        |\n");
                printf("| 6.deleteFirst    |\n");
                printf("| 7.deleteLast     |\n");
                printf("| 8.deleteAtPos    |\n");
                printf("--------------------\n");

                int ch;
                printf("\nEnter choice : ");
                scanf("%d",&ch);
                switch(ch){
                        case 1:
                                addNode();
                                break;
                        case 2:
                                addFirst();
                                break;
                        case 3:
                                addLast();
                                break;
                        case 4:
                                {
                                int pos;
                                printf("Enter Position want to insert Node : ");
                                scanf("%d",&pos);
                                addAtPos(pos);
                                }
                                break;
                        case 5:
                                display();
                                break;
			case 6:
				deleteFirst();
				break;
			case 7:
				deleteLast();
				break;
			case 8:
				{
				int pos;
				printf("Enter Position want to delete : ");
				scanf("%d",&pos);
				deleteAtPos(pos);
				}
                        default:
                                printf("Invalid choide BSDK\n");
                }

                getchar();
                printf("Do you want to continue (y/n): ");
                scanf("%c",&choice);
        }while(choice == 'Y' || choice == 'y');
	printf("DHANYAWAD\n");

}
