#include<stdio.h>
#include<stdlib.h>
typedef struct Employee{
    char name[10];
    int id;
    struct Employee *next;
}emp;

emp *head=NULL;

emp* createNode(){
    emp *newNode=(emp*)malloc(sizeof(emp));
    printf("ENter name of emp:\n");
    char ch;
    int i=0;
    while((ch=getchar())!='\n'){
        (*newNode).name[i]=ch;
        i++;

    }

    printf("ENter id:\n");
    scanf("%d",&newNode->id);
    getchar();
    newNode->next=NULL;
    return newNode;
}

void addNode(){
    emp *newNode=createNode();
    if(head==NULL){
        head=newNode;
    }else{
        emp *temp=head;
        while(temp->next!=NULL){
            temp=temp->next;
        }
        temp->next=newNode;
    }
}

void addFirst(){
    emp* newNode=createNode();
    if(head==NULL){
        head=newNode;
    }else{
        newNode->next=head;
        head=newNode;
    }
}

void count(){
    emp *temp=head;
    int count=0;

    while(temp!=NULL){
        count++;
        temp=temp->next;
    }
    printf("count=%d\n",count);
}

void addatPos(){
    emp *newNode=createNode();
    emp *temp=head;
    int pos=3;
    while(pos-2){
        temp=temp->next;
        pos--;
    }
    newNode->next=temp->next;
    temp->next=newNode;
}

void printLL(){
    emp* temp=head;
    while(temp!=NULL){
        printf("|%s->",temp->name);
        printf("%d|\n",temp->id);
        temp=temp->next;
    }
}

void main(){
    int nodecount;
    printf("ENter node count:\n");
    scanf("%d",&nodecount);
    getchar();

    for(int i=0;i<nodecount;i++){
        addNode();
    }

    addFirst();
    addatPos();
    count();
    printLL();
}


