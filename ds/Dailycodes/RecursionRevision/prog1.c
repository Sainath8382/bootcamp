//sum till the given number from 1 using recursion

#include<stdio.h>

void sumno(int num){

	static int sum=0;
	sum=sum+num;

	if(num!=1)
		sumno(--num);
	else
		printf("%d\n",sum);
}

void main(){

	sumno(10);
}
