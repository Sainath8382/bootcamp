//Factorial using recursion

#include<stdio.h>

void factorial(int num){

	static int fact=1;
	fact=fact*num;

	if(num!=1)
		factorial(--num);
	else
		printf("%d\n",fact);
}

void main(){

	factorial(5);
}
