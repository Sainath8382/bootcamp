#include<stdio.h>

//print nos in  reverse from given no with recursion

void fun(int x){
	printf("%d\n",x);
	if(x!=1)
		fun(--x);
	
}

void main(){
	fun(10);
}
