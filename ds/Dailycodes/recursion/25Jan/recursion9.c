
#include<stdio.h>

//factorial with recursion

int fact(int num){
	static int prod=1;
	prod=prod*num;
	if(num!=1)
		fact(--num);
	
//	printf("Factorial is %d\n",prod);
	return prod;
}

void main(){
	int factorial = fact(5);
	printf("Factorial is %d\n",factorial);
}


