//Factorial proper way

#include<stdio.h>
int factorial(int N){
	if(N==1){
		return 1;
	}
	return factorial(N-1)*N;
}

void main()
{
	int ret=factorial(4);
	printf("Factorial is %d\n",ret);
}
