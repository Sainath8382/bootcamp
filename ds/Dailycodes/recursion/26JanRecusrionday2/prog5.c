//Factorial using recursion proper way without any extra variable

#include<stdio.h>

int factorial(int num){
	if(num<=1){
		return 1;
	}

	return factorial(num-1)*num;
}

void main(){
       int fact = factorial(5);
       printf("Factorial of given number is %d\n",fact);
}
