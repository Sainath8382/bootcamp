#include<stdio.h>
//Factorial using recursion

int factorial(int num){
	static int fact=1;  //or declare as global
	fact=fact*num; 
	if(num!=1){
		factorial(--num);
	}
	return fact;
}

void main(){
	int fact = factorial(5);
	printf("Factorial is %d\n",fact);
}
