//factorial 2

#include<stdio.h>
int factorial(int num){
	static int fact=1;
	if(num==1){
		return fact;
	}
	fact=fact*num;
	return  factorial(--num);
}

void main(){
	int x=factorial(5);
	printf("Factorial is %d\n",x);
}
