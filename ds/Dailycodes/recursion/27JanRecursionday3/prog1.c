//Sum using recursion

#include<stdio.h>

int fun(int num){
	if(num<=1)
	return 1;

	return num+fun(num-1)+fun(num-2);
}

void main(){
	int sum=fun(4);
	printf("Sum=%d\n",sum);
}
