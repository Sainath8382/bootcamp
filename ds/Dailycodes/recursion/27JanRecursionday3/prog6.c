//Palindrome check using recursion

#include<stdio.h>
#include<stdbool.h>

bool ispalindrome(char carr[],int start,int end){
	if(carr[start]!=carr[end]){
		return false;
	}else if(carr[start]==carr[end] && start<end){
		return ispalindrome(carr,start+1,end-1);
	}
		return true;
}

void main(){
	char carr[]={'m','a','l','a','y','a','l','a','m'};
	int size=9;
	
	if(ispalindrome(carr,0,size-1)){
		printf("is palindrome\n");
	}
	else{
		printf("Not a palindrome\n");
	}

}
