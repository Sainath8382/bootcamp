//Recursion in character array
//Finding a character index in that array

#include<stdio.h>

int searcharr(int arr[],int size,char ch){
	if(size==0){
		return -1;
	}else if(arr[size-1]==ch){
		return size-1;
	}
	return searcharr(arr,size-1,ch);
}

void main(){
	int arr[5]={'A','B','C','D','E'};
	int size=5;

	int ret=searcharr(arr,size,'F');
	
	if(ret==-1){
		printf("Element not found\n");
	}else{
		printf("The given character is at index %d\n",ret);
	}
}
