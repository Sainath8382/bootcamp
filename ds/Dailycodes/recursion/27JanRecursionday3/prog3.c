//Fibonacci series problem using recursion
#include<stdio.h>

int fibo(int num){
	if(num==0){
		return 0;
	}else if(num==1){
		return 1;
	}
	return fibo(num-1)+fibo(num-2);
}

void main(){
	int num;
	printf("ENter the number for corresponding fibo series:\n");
	scanf("%d",&num);

	int ret=fibo(num);
	printf("The corresponding fibonacci number in series is %d\n",ret);
}

