//Sum of array elements using recursion


#include<stdio.h>

int sumarr(int arr[],int size){
	if(size==0){
		return 0;
	}
	return sumarr(arr,size-1)+arr[size-1];
}

//second approach using one less iteration

int arrsum2(int arr[],int size){
	 if(size==1)
		 return arr[size-1];
		// return size-1;		both work properly
	
	return sumarr(arr,size-1)+arr[size-1];
}

void main(){
	int size=5;
	int arr[]={10,20,30,40,50};
	
	int ret=sumarr(arr,size);
	printf("Sum is %d\n",ret);
	
	int arr2[]={1,2,3,4,5};
	int ret2=sumarr(arr2,size);
	printf("Sum 2 is %d\n",ret2);
}
