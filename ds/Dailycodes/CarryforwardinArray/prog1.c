//Left max element using carryforward in array

#include<stdio.h>

void leftmax(int arr[],int N){
	int leftmax[N];

	for(int i=0;i<N;i++){
		int max=arr[i];
		for(int j=0;j<=i;j++){
			if(max<arr[j]){
				max=arr[j];
			}
		}
		leftmax[i]=max;
	}

	for(int i=0;i<N;i++){
		printf("%d\t",leftmax[i]);
	}
	printf("\n");
}

void main(){
	int arr[]={5,2,1,-4,-2,9,3,4,7};
	int N=9;

	leftmax(arr,N);
}

