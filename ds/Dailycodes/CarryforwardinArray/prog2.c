//Left max element using carryforward in array

#include<stdio.h>

void leftmax(int arr[],int N){
	int leftmax[N];

	leftmax[0]=arr[0];
	for(int i=1;i<N;i++){
		if(arr[i]>leftmax[i-1])
			leftmax[i]=arr[i];
		else
			leftmax[i]=leftmax[i-1];
	}

	for(int i=0;i<N;i++){
		printf("%d\t",leftmax[i]);
	}
	printf("\n");
}

void main(){
	int arr[]={5,2,1,-4,-2,9,3,4,7};
	int N=9;

	leftmax(arr,N);
}

