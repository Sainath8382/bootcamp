//Leaders in array

#include<stdio.h>
void leader(int *arr,int N){
	for(int i=0;i<N;i++){
		int leader=arr[i];
		for(int j=i+1;j<N;j++){
			if(arr[j]>=leader){
				leader=arr[j];
			}
		}
		printf("%d\t",leader);	
	}
	printf("\n");
}

void main(){
	int arr[]={10,17,4,3,5,2};
	int N=6;
	leader(arr,N);
}
