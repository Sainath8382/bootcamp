//Rightmax using carryforward

#include<stdio.h>

void rightmax(int arr[],int N){
	int rmax[N];

	for(int i=N-1;i>=0;i--){
		int max=arr[i];
		for(int j=N-1;j>=i;j--){
			if(arr[j]>max){
				max=arr[j];
			}
		}
		rmax[i]=max;
		
	}
	

	for(int i=0;i<N;i++){
		printf("%d\t",rmax[i]);
	}
}

void main(){
	int arr[]={5,2,5,-4,-2,9,3,4,7};
	int N=9;

	rightmax(arr,9);
}


