//implement all functions
//create node   add node    countnode   addFirst    addatposition   printLL

#include<stdio.h>
#include<stdlib.h>

struct Demo{
    int data;
    struct Demo *next;
};

struct Demo *head=NULL;

struct Demo* createNode(){
    struct Demo *newNode=(struct Demo*)malloc(sizeof(struct Demo));
    printf("ENter data:\n");
    scanf("%d",&newNode->data);
    newNode->next=NULL;

    return newNode;
}

void addNode(){
    struct Demo *newNode=createNode();
    if(head==NULL){
        head=newNode;
    }else{
        struct Demo *temp=head;
        while(temp->next!=NULL){
            temp=temp->next;
        }
        temp->next=newNode;
    }
}

void addFirst(){
    struct Demo *newNode=createNode();
    if(head==NULL){
        head=newNode;
    }else{
        newNode->next=head;
        head=newNode;
    }
}

void countnode(){
    struct Demo *temp=head;
    int count=0;
    while(temp!=NULL){
        count++;
        temp=temp->next;
    }
    printf("%d",count);
}

void printLL(){
    struct Demo *temp=head;
    while(temp!=NULL){
        printf("|%d|",temp->data);
        printf("\n");
        temp=temp->next;
    }
}

void addatpos(int pos){
    struct Demo *newNode=createNode();
    struct Demo *temp=head;
    //pos=3;
    while(pos-2){
        temp=temp->next;
        pos--;
    }
    newNode->next=temp->next;
    temp->next=newNode;
}

void main(){
    int nodecount;
    printf("ENter node count:\n");
    scanf("%d",&nodecount);
    //getchar();

    for(int i=0;i<nodecount;i++){
        addNode();
    }
    printLL();
    addFirst();

    printLL();
    countnode();

    int pos;
    printf("Enter position:\n");
    scanf("%d",&pos);
    addatpos(pos);

    printLL();
}
