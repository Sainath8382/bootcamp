#include<stdio.h>
#include<stdlib.h>

typedef struct Employee{
    char ename[10];
    int eid;
    struct Employee *next;
}emp;

emp* createNode(){
    emp *newNode=(emp*)malloc(sizeof(emp));
    printf("Enter name:\n");
    char ch;
    int i=0;

    while((ch=getchar())!='\n'){
        (*newNode).ename[i]=ch;
        i++;
    }

    printf("Enter id:\n");
    scanf("%d",&newNode->eid);

    newNode->next=NULL;
    return newNode;
}

emp *head=NULL;

void addNode(){
    emp *newNode=createNode();

    if(head==NULL){
        head=newNode;
    }else{
        emp *temp=head;
        while(temp->next!=NULL){
            temp=temp->next;
        }
        temp->next=newNode;
    }
}

void addFirst(){
    emp *newNode=createNode();
    if(head==NULL){
        head=newNode;
    }else{
        newNode->next=head;
        head=newNode;
    }
}

void count(){
    emp *temp=head;
    int count=0;
    while(temp!=NULL){
        count++;
        temp=temp->next;
    }
    printf("count=%d\n",count);
}

void addLast(){
    addNode();
}

void addatPos(int pos){
    emp *newNode=createNode();
    emp *temp=head;
    while(pos-2){
        temp=temp->next;
        pos--;
    }
    newNode->next=temp->next;
    temp->next=newNode;
}

void printLL(){
    emp *temp=head;
    while(temp!=NULL){
        printf("|%s ",temp->ename);
        printf("%d|->",temp->eid);
        temp=temp->next;
    }
}

void main(){
    char choice;

    do{
        printf("1.addNode\n");
        printf("2.addFirst\n");
        printf("3.addLast\n");
        printf("4.addatPos\n");
        printf("5.count\n");
        printf("6.printLL\n");

        int ch;
        printf("Enter your choice:\n");
        scanf("%d",&ch);
        getchar();

        switch(ch){
            case 1:
                addNode();
                break;

            case 2:
                addFirst();
                break;
            case 3:
                addLast();
                break;
            case 4:
                {
                int pos;
                printf("ENter the position:\n");
                scanf("%d",&pos);
                getchar();

                addatPos(pos);
                }
                break;

            case 5:
                count();
                break;

            case 6:
                printLL();
                break;
            default:
                printf("Wrong choice\n");
        }
        getchar();
        printf("Do you want to continue:\n");
        scanf("%c",&choice);
    }while(choice=='Y' || choice=='y');
}
