//realtime example for singly LL

#include<stdio.h>
#include<stdlib.h>

typedef struct Society{
    char wing[1];
    int floors;
    struct Society *next;
}Soc;

Soc *createNode(){
    Soc *newNode=(Soc*)malloc(sizeof(Soc));
    getchar();

    printf("ENter wing:\n");
    char ch;
    int i=0;

    while((ch=getchar())!='\n' && i!=2){
        (*newNode).wing[i]=ch;

    }

    printf("Enter floors:\n");
    scanf("%d",&newNode->floors);

    newNode->next=NULL;

    return newNode;
}

Soc *head=NULL;

void addNode(){
    Soc *newNode=createNode();
    if(head==NULL){
        head=newNode;
    }else{
        Soc *temp=head;
        while(temp->next!=NULL){
            temp=temp->next;
        }
        temp->next=newNode;
    }
}

void addFirst(){
    Soc *newNode=createNode();
    if(head==NULL){
        head=newNode;
    }else{
        newNode->next=head;
        head=newNode;
    }
}

void addLast(){
    addNode();
}

int nodecount(){
    Soc *temp=head;
    int count=0;
    while(temp!=NULL){
        count++;
        temp=temp->next;
    }
    printf("Count=%d\n",count);
    return count;
}

int addatPos(int pos){
    int count=nodecount();

    if(pos<=0 ||pos>=count+2){
        printf("Invalid node position\n");
        return -1;
    }else{
        if(pos==1){
            addFirst();
        }else if(pos==count+1){
            addLast();
        }else{
            Soc *newNode=createNode();
            Soc *temp=head;
            while(pos-2){
                temp=temp->next;
                pos--;
            }
            newNode->next=temp->next;
            temp->next=newNode;
        }
    }
    return 0;
}

void delFirst(){
    if(head==NULL){
        printf("Invalid operation\n");
    }else if(head->next=NULL){
        free(head);
        head=NULL;
    }else{
        Soc *temp=head;
        head=temp->next;
        free(temp);
    }
}

void delLast(){
    Soc *temp=head;
    if(head->next==NULL){
        free(temp);
        head=NULL;
    }else{
        Soc *temp=head;
        while(temp->next->next!=NULL){
            temp=temp->next;
        }
        free(temp->next);
        temp->next=NULL;
    }
}

int delatPos(int pos){
    int count=nodecount();
    if(pos<=0 || pos>count){
        printf("Invalid operation\n");
        return -1;
    }else{
        if(pos==1){
            delFirst();
        }else if(pos==count){
            delLast();
        }else{
            Soc *temp=head;

            while(pos-2){
                temp=temp->next;
                pos--;
            }
            Soc *temp2=temp->next;
            temp->next=temp2->next;
            free(temp2);
        }
    }
    return 0;
}

void printLL(){
    Soc *temp=head;

    while(temp->next!=NULL){
        printf("|%s :",temp->wing);
        printf(" %d|->",temp->floors);
        temp=temp->next;
    }
    printf("|%s : %d|\n",temp->wing,temp->floors);
}

void main(){
    char choice;

    do{
        printf("1.addNode\n");
        printf("2.addFirst\n");
        printf("3.addLast\n");
        printf("4.addatPos\n");
        printf("5.delFirst\n");
        printf("6.delLast\n");
        printf("7.delatPos\n");
        printf("8.printLL\n");

        int ch;
        printf("Enter your choice:\n");
        scanf("%d",&ch);
        switch(ch){
                        case 1:
                addNode();
                break;

            case 2:
                addFirst();
                break;

            case 3:
                addLast();
                break;

            case 4:{
                int pos;
                printf("Enter the pos:\n");
                scanf("%d",&pos);

                addatPos(pos);
                }
                break;

             case 5:
                delFirst();
                break;

             case 6:
                delLast();
                break;

             case 7:{
                int pos;
                printf("Enter the pos:\n");
                scanf("%d",&pos);

                delatPos(pos);
                }
                break;

            case 8:
                printLL();
                break;

        }
        getchar();
        printf("Do you want to continue:\n");
        scanf("%c",&choice);
        getchar();
    }while(choice=='y' || choice=='Y');
}
