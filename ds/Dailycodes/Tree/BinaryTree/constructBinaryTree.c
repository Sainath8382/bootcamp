//Given '2' arrays having tree data the arrays are "inOrder" and "preOrder"
//From the data given by these 2 arrays construct a binary tree and print it

#include<stdio.h>
#include<stdlib.h>

struct TreeNode{

	int data;
	struct TreeNode *left;
	struct TreeNode *right;
};

struct TreeNode *constructBT( int inOrder[] ,int preOrder[], int inStart, int inEnd, int preStart, int preEnd){

	if(inStart > inEnd){
		return NULL;
	}

	struct TreeNode *rootNode = (struct TreeNode*)malloc(sizeof(struct TreeNode));
	rootNode->data = preOrder[preStart];

	int root = preOrder[preStart];
	int index;

	for(index = inStart ; index < inEnd; index++){

		if(inOrder[index] == root)
			break;
	}

	int Llen = index - inStart;

	rootNode->left = constructBT( inOrder, preOrder, inStart, index-1  , preStart+1, preStart+Llen);

	rootNode->right = constructBT(inOrder,preOrder, index+1,inEnd, preStart+Llen+1 , preEnd);

	return rootNode;
}

void preOrder(struct TreeNode *root){

	if(root == NULL)
		return;

	printf("%d ",root->data);
	preOrder(root->left);
	preOrder(root->right);
}

void inOrder( struct TreeNode *root){
	
	if(root == NULL)
		return;
	inOrder(root->left);
	printf("%d ", root->data);
	inOrder(root->right);
}

void postOrder(struct TreeNode *root){

	if(root == NULL)
		return;

	postOrder(root->left);
	postOrder(root->right);
	printf("%d ",root->data);
}

void main(){

	int inOrderarray[] = {4,2,5,1,6,7,3};
	int preOrderarray[] = {1,2,4,5,3,6,7};

	int size = 7;
	//int size = sizeof(inOrderarray)/sizeof(inOrderarray[0]);
	int inStart = 0, inEnd = size-1;
	int preStart = 0, preEnd = size-1;

	struct TreeNode *root = constructBT(inOrderarray,preOrderarray,inStart,inEnd,preStart, preEnd);
	preOrder(root);
	printf("\n");
	inOrder(root);
	printf("\n");
	postOrder(root);

}
	
