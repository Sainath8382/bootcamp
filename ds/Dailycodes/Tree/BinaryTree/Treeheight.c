//return the sum of the tree elements

#include<stdio.h>
#include<stdlib.h>

typedef struct TreeNode{
	int data;
	struct TreeNode *left;
	struct TreeNode *right;
}tree;

tree *createNode(int level){

	level++;
	tree *newNode = (tree*)malloc(sizeof(tree));
	printf("Enter data for level %d\n",level);
	scanf("%d",&newNode->data);

	getchar();
	char ch;
	printf("continue for level %d left side\n",level);
	scanf("%c",&ch);
	if(ch == 'Y' || ch == 'y')
		newNode ->left = createNode(level);
	else
		newNode->left = NULL;

	getchar();
	char ch1;
	printf("continue for level %d right side\n",level);
	scanf("%c",&ch1);

	if(ch1 == 'Y' || ch1 == 'y')
		newNode->right = createNode(level);
	else
		newNode->right = NULL;

	return newNode;
}


void preOrder(tree *root){
	if(root == NULL)
		return ;

	printf("|%d ",root->data);
	preOrder(root->left);
	preOrder(root->right);
}

void inOrder(tree *root){

	if(root==NULL)
		return ;
	
	inOrder(root->left);
	printf("|%d |",root->data);
	inOrder(root->right);
}

void postOrder(tree *root){

	if(root == NULL)
		return;

	postOrder(root->left);
	postOrder(root->right);
	printf("|%d |",root->data);
}

void printTree(tree *root){
	
        if(root == NULL)
                printf("Tree is empty\n");
		
        char ch;
        do{
                printf("1. preOrder\t");
                printf("2. inOrder\t");
                printf("3. postOrder\t");
											
                printf("\n");
                int ch1;
                printf("Enter choice :\n");
                scanf("%d",&ch1);
																
                switch(ch1){
																				
                	case 1:
		             preOrder(root);
				printf("\n");
			     break;
		       case 2:
			     inOrder(root);
		       		printf("\n");	
			     break;	
																														  
		        case 3:
		             postOrder(root);
			     printf("\n");
		             break;
	              	 }

			getchar();			
		       	printf("\n");
			printf("continue Y or N ?\n");	
			scanf("%c",&ch);

	}while(ch=='Y' || ch=='y');
}

int max(int lh,int rh){
	
	if(lh>rh)
		return lh;
	else
		return rh;
}

int Treeheight(tree *root){

	if(root == NULL)
		return -1;

	int leftheight = Treeheight(root->left);
	int rightheight = Treeheight(root->right);

	return max(leftheight,rightheight) + 1;
}

void main(){

	int level = 0;
	tree *root = (tree*)malloc(sizeof(tree));
	printf("Enter data for root\n");
	scanf("%d",&root->data);
	
	getchar();
	char ch;
	printf("Continue for rootleft side\n");
	scanf("%c",&ch);

	if(ch == 'Y' || ch == 'y')
		root->left = createNode(level);
	else
		root->left = NULL;

	getchar();
	char ch1;
	printf("Continue for rootright side\n");
	scanf("%c",&ch1);

	if(ch1== 'Y' || ch1 == 'y')
		root->right = createNode(level);
	else
		root->right = NULL;

	printTree(root);

	int height = Treeheight(root);
	printf("The height of tree is %d\n",height);
}


