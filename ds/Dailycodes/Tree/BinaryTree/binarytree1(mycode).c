//Binary tree : It is a tree in which the nodes have at the max 2 children 

#include<stdio.h>
#include<stdlib.h>

struct Node{

	int data;
	struct Node *left;
	struct Node *right;
};

void createNode(struct Node* root){

	printf("Enter data: ");
	scanf("%d",&root->data);
	root->left = NULL;
	root->right = NULL;

	char ch;
	printf("Enter your choice for left side: ");
	scanf("%c",&ch);
	getchar();
	
	if(ch == 'y' || ch =='Y'){
		struct Node *newNode = (struct Node*)malloc(sizeof(struct Node));
		createNode(newNode);
		root->left = newNode;
	}else{
		root->left = NULL;
	}

	char ch1;
	printf("Enter your choice for right side: ");
	scanf("%c",&ch1);
	getchar();

	if(ch1=='y' || ch1 == 'Y'){
		struct Node *newNode = (struct Node*)malloc(sizeof(struct Node));
		createNode(newNode);
		root->right = newNode;
	}
	else{
		root->right = NULL;
	}
}


void main(){

	struct Node *root = (struct Node*)malloc(sizeof(struct Node));
	createNode(root);
}











































