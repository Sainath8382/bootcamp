//Binary tree is a tree in which each node has at max 2 chidren.
//3 traversals here : preOrder, inOrder , postOrder.

#include<stdio.h>
#include<stdlib.h>

struct TreeNode{

	int data;
	struct TreeNode *left ;
	struct TreeNode *right;
};

struct TreeNode *createNode(int level){

	level = level+1;
	struct TreeNode *newNode = (struct TreeNode*)malloc(sizeof(struct TreeNode));

	printf("Enter data for level %d\n",level);
	scanf("%d",&newNode->data);
	getchar();

	char ch;
	printf("Enter choice for left side for level %d\n",level);
	scanf("%c",&ch);

	if(ch=='Y' || ch == 'y')
		newNode->left = createNode(level);
	else
		newNode->left = NULL;

	getchar();

	char ch2;
	printf("Enter choice for right side for level %d\n",level);
	scanf("%c",&ch2);
	if(ch=='Y' || ch == 'y')
		newNode->right = createNode(level);
	else
		newNode->right = NULL;

	return newNode;
}

void preOrder(struct TreeNode *root){

	if(root == NULL)
		return;
	
	printf("%d\t",root->data);
	preOrder(root->left);
	preOrder(root->right);

	
}

void inOrder(struct TreeNode *root){
	
	if(root == NULL)
		return;

	inOrder(root->left);
	printf("%d\t",root->data);
	inOrder(root->right);

}

void postOrder(struct TreeNode *root){
	
	if(root == NULL)
		return;

	postOrder(root->left);
	postOrder(root->right);
	printf("%d\t",root->data);
}

void printTree(struct TreeNode *root){

	if(root == NULL)
		printf("Tree is empty\n");

	char ch;
	do{
		printf("1. preOrder\t");
		printf("2. inOrder\t");
		printf("3. postOrder\t");
		
		printf("\n");
		int ch1;
		printf("Enter choice :\n");
		scanf("%d",&ch1);

		switch(ch1){

			case 1: 
				preOrder(root);
				break;

			case 2:
				inOrder(root);
				break;

			case 3:
				postOrder(root);
				break;
		}
		getchar();
		printf("\n");
		printf("continue ?\n");
		scanf("%c",&ch);

	}while(ch=='Y' || ch=='y');

}

void main(){

	struct TreeNode *root = (struct TreeNode*)malloc(sizeof(struct TreeNode));
	printf("Enter data for rootnode: ");
	scanf("%d",&(root->data));

	printf("\t\t\tTree is rooted with %d\n",root->data);
	int level = 0;
	getchar();

	char ch1;
	printf("Enter choice for root left side for level %d\n",level);
	scanf("%c",&ch1);

	if(ch1 == 'Y' || ch1 == 'y')
		root->left = createNode(level);
	else
		root->left = NULL;

	getchar();
	char ch2;
	printf("Enter choice for root right side for level %d\n",level);
	scanf("%c",&ch2);

	if(ch2=='y' || ch2 == 'Y')
		root->right = createNode(level);
	else
		root->right = NULL;

	printf("Tree output\n");
	printTree(root);
}
