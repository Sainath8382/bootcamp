//Doubly Linked List

#include<stdio.h>
#include<stdlib.h>

typedef struct Node{
    struct Node *prev;
    int data;
    struct Node *next;
}Node;


Node *createNode(){
    Node *newNode=(Node*)malloc(sizeof(Node));
    newNode->prev=NULL;
    printf("ENter data:\n");
    scanf("%d",&newNode->data);
    newNode->next=NULL;

    return newNode;
}

Node *head=NULL;

void addNode(){
    Node *newNode=createNode();
    if(head==NULL){
        head=newNode;
    }else{
        Node *temp=head;
        while(temp->next!=NULL){
            temp=temp->next;
        }
        temp->next=newNode;
        newNode->prev=temp;
    }
}

void addFirst(){
    if(head==NULL){
        addNode();
    }else{
        Node *newNode=createNode();
        newNode->next=head;
        head->prev=newNode;
        head=newNode;
    }
}

void addLast(){
    addNode();
}

int nodecount(){
    Node *temp=head;
    int count=0;
    while(temp!=NULL){
        count++;
        temp=temp->next;
    }
   // printf("%d\n",count);
    return count;
}

int addatPos(int pos){
    int count =nodecount();
    if(pos<=0 || pos>=count+2){
        printf("Invalid node operation\n");
        return -1;
    }else{
        if(pos==1){
            addFirst();
        }else if(pos==count+1){
            addNode();
        }
        else{

            Node *newNode=createNode();
            Node *temp=head;
            while(pos-2){
                temp=temp->next;
                pos--;
            }
            newNode->next=temp->next;
            newNode->prev=temp;
            temp->next->prev=newNode;
            temp->next=newNode;
        }return 0;
    }
}


int delFirst(){

    if(head==NULL){
        printf("Linked List is empty\n");
        return -1;
    }else{
            head=head->next;
            free(head->prev);
            head->prev=NULL;
            return 0;
        }
}

int delLast(){

    if(head==NULL){
        printf("LL empty");
        return -1;
    }else{
        if(head->next==NULL){
            delFirst();
        }else{
            Node *temp=head;
            while(temp->next->next!=NULL){
                temp=temp->next;
            }
            free(temp->next);
                temp->next=NULL;
        } return 0;
    }
}

int delatPos(int pos){
    //Node *temp=head;
    int count=nodecount();
    if(pos<=0 || pos>count){
        printf("Invalid operation\n");
        return -1;
    }else{
        if(pos==1){
            delFirst();
        }else if(pos==count){
            delLast();
        }else{
            Node *temp=head;
            while(pos-2){
                temp=temp->next;
                pos--;
            }
            temp->next=temp->next->next;
            free(temp->next->prev);
            temp->next->prev=temp;
        }
        return 0;
    }
}

int printDLL(){

    if(head==NULL){
        printf("Linked List is empty\n");
        return -1;
    }else{
        Node *temp=head;
        while(temp->next!=NULL){
            printf("|%d|->",temp->data);
            temp=temp->next;
        }
        printf("|%d|",temp->data);
        return 0;
    }
}

void main(){
    char choice;

    do{
        printf("1.addNode\n");
        printf("2.addFirst\n");
        printf("3.addLast\n");
        printf("4.count\n");
        printf("5.addatPos\n");
        printf("6.printDLL\n");
        printf("7.deleteFirst\n");
        printf("8.deleteLast\n");
        printf("9.delatPos\n");

        int ch;
        printf("ENter your choice:\n");
        scanf("%d",&ch);

        switch(ch){
            case 1:
                addNode();
                break;

            case 2:
                addFirst();
                break;

            case 3:
                addLast();
                break;

            case 4:
                nodecount();
                break;

            case 5:{
                int pos;
                printf("Enter the pos:\n");
                scanf("%d",&pos);

                addatPos(pos);
                }
                break;

            case 6:
                printDLL();
                break;

            case 7:
                delFirst();
                break;

            case 8:
                delLast();
                break;

             case 9:{
                int pos;
                printf("Enter the pos:\n");
                scanf("%d",&pos);

                delatPos(pos);
                }
                break;
             default:
                printf("Wrong choice\n");
        }
        getchar();
        printf("Do you want to continue:\n");
        scanf("%c",&choice);
    }while(choice=='y' || choice=='Y');
}


