//Structure pointer

#include<stdio.h>
struct Movie{
    char mname[10];
    int count;
    float rating;
}obj1={"Drishyam",5,8.8};

void main(){
        typedef struct Movie mv;
        struct Movie *ptr=&obj1;
        mv obj2={"Knight",4,8.6};
        mv *ptr2=&obj2;

        printf("%s\n",(*ptr).mname);
        printf("%d\n",(*ptr).count);
        printf("%f\n",(*ptr).rating);

        printf("%s\n",ptr2->mname);
        printf("%d\n",ptr2->count);
        printf("%f\n",ptr2->rating);
}
