//Kadane's algorithm : Kadane's algorithm is used such that if the value of the sum is less than 0 then it is initialized as 0 as negative value does not add any value while doing addition

#include<stdio.h>

void Kadane(int *arr,int N){
	int max=arr[0];
	int sum=0;

	for(int i=0;i<N;i++){
		
			int sum=sum + arr[i];

			if(sum<0)
				sum = 0;

			if(max<sum)
				max=sum;
		
	}
	printf("The max subarray value is %d\n",max);
}

void main(){
	int arr[]={-2,1,-3,4,-1,2,1,-5,4};
	int N=9;

	Kadane(arr,N);
}
