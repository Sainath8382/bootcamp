//Max subarray BruteForce Approach

#include<stdio.h>

void maxsub(int *arr,int N){
	int maxsum=0;

	for(int i=0;i<N;i++){
		
		for(int j=i;j<N;j++){
			int sum=0;
			for(int k=i;k<=j;k++){
				sum =sum + arr[k];
			}
			if(maxsum<sum)
				maxsum=sum;
		}

	}
	printf("The maximum sum of subarray is %d\n",maxsum);
}

void main(){
	int arr[]={-2,1,-3,4,-1,2,1,-5,4};
	int N=9;

	maxsub(arr,N);	
}
