//Max sub array BruteForce Approach 2 : The previous code has time complexity of O(N)^3; Here we reduce the complexity to O(N)^2

#include<stdio.h>

void maxsub(int *arr,int N){
	int maxsum=0;

	for(int i=0;i<N;i++){
		
		for(int j=i;j<N;j++){
			
			int sum=sum+arr[j];
			
			if(maxsum<sum)
				maxsum=sum;
		}	
	}
	printf("The sum of max subarray is %d\n",maxsum);
}

void main(){
	int arr[]={-2,1,-3,-1,2,1,-5,4};
	int N=9;

	maxsub(arr,N);
}
