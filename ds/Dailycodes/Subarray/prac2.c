//Counting sub arrays:
//expln: array is given of positive integers and an extra value B is given print the count of the sub arrays if the sum of sub arrays is less than B.
// example: arr[]={2,5,6},B=10;  O/p : 4

#include<stdio.h>

void count(int *arr,int size,int B){
	int count=0;

	for(int i=0;i<size;i++){
		int sum=0;
		for(int j=i;j<size;j++){
			sum = sum+arr[j];

			if(sum<B)
				count++;

		}	
	}
	printf("%d\n",count);
}

void main(){
	int arr[]={1,11,2,3,15};
	int N=5;
	int B=10;

	count(arr,N,B);
}
