//SubArray
//1.Print all sub arrays

#include<stdio.h>

void subarr(int *arr,int N){
	for(int i=0;i<N;i++){
		for(int j=i;j<N;j++){
			for(int k=i;k<=j;k++){
				printf("%d\t",arr[k]);
			}
			printf("\n");
		}
		printf("\n");
	}
}

void main(){
	int arr[]={4,2,1,3,5};
	int N=5;
	subarr(arr,N);
}
