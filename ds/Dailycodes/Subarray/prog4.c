//Sum of every single sub-array using 2 for loops

#include<stdio.h>

void sumsubarr(int *arr,int N){
	
	for(int i=0;i<N;i++){
		int sum=0;
		for(int j=i;j<N;j++){
			
			sum=sum+arr[j];
			printf("%d\n",sum);
		}
		printf("\n");
	}
}

void main(){
	int arr[]={2,4,1,3};
	int N=4;
	sumsubarr(arr,N);
}
