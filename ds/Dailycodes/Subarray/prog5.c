//print sum of all sub arrays using array prefix

#include<stdio.h>
void sumsub(int *arr,int N){
	int psum[N];
	psum[0]=arr[0];

	for(int i=0;i<N;i++){
		psum[i]=psum[i-1]+arr[i];
	}

	for(int i=0;i<N;i++){
		for(int j=i;j<N;j++){
			int sum=0;
			if(i==0)
				sum=psum[j];
			else
				sum = psum[j]-psum[i-1];
			
			printf("%d\n",sum);
		}
	}
}

void main(){
	int arr[]={2,4,1,3};
	int N=4;

	sumsub(arr,N);
}	
