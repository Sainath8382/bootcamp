//Prefix sum : pre-requisite code for sub array

#include<stdio.h>
void prefix(int *arr,int N){
	int pSum[5];
	pSum[0]=arr[0];

	for(int i=1;i<N;i++){
		pSum[i]=pSum[i-1]+arr[i];
	}

	printf("The prefixed sum array is\n");
	for(int i=0;i<N;i++){
		printf("%d\t",pSum[i]);
	}
	printf("\n");
}

void main(){
	int arr[]={1,2,3,4,5};
	int N=5;

	prefix(arr,N);
}	
