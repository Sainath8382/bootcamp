//Insertion sort : Insertion sort approach is in a way such that the array is divided into two types sorted and unsorted at each iteration and sorting is performed.

#include<stdio.h>
void merge(int *arr,int size){
	for(int i=1;i<size;i++){
		int val=arr[i];
		int j=i-1;
		for( ;arr[j]>val && j>=0;j--){
			arr[j+1]=arr[j];
		}
		arr[j+1]=val;
	}
}

void main(){
	int arr[]={4,-2,7,1,-5,8};
	int size=sizeof(arr)/sizeof(arr[0]);

	printf("The given array is :\n");
	for(int i=0;i<size;i++){
		printf("%d\t",arr[i]);
	}
	printf("\n");

	merge(arr,size);

	printf("The sorted array is :\n");
	for(int i=0;i<size;i++){
		printf("%d\t",arr[i]);
	}
	printf("\n");
}
