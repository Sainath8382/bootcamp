//Insertion sort :  In insertion sort we divide the array into 2 parts sorted array and unsorted array. Firstly we consider the first element as sorted and rest unsorted further on we sort and increase the elements in the sorted array.

#include<stdio.h>

void insertion(int arr[],int size){
	int start=0,end=size-1;

	for(int i=1;i<size;i++){
		int val=arr[i];
		int j=i-1;
		for(;j>=0 && arr[j]>val;j--){
			arr[j+1]=arr[j];
		}
		arr[j+1]=val;
	}
}

void main(){
	int arr[]={5,3,-4,1,7,-6};
	int size=6;

	insertion(arr,size);

	printf("The sorted array is \n");
	for(int i=0;i<size;i++){
		printf("%d\t",arr[i]);
	}	
	printf("\n");
}


