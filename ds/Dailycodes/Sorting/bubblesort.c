//Bubble sort : The idea of bubble sort is that the largest element in the array should be at the last position 
// Bubble sort entirely deals with the last position of the array

#include<stdio.h>

void bubble(int *arr,int size){
	int start=0,end=size-1;
	for(int i=0;i<size;i++){
		for(int j=0;j<size-i-1;j++){
			if(arr[j]>arr[j+1]){
				int temp=arr[j];
				arr[j]=arr[j+1];
				arr[j+1]=temp;
			}
		}
	}	
}

void main(){
	int arr[]={3,-4,2,7,1,-5};
	int size=6;

	bubble(arr,size);
	printf("The sorted array is \n");

	for(int i=0;i<size;i++){
		printf("|%d ",arr[i]);
	}
	printf("|\n");
}
