//Bubble sort 1

#include<stdio.h>
void sort1(int arr[],int size){
	for(int i=0;i<=size-1;i++){
		for(int j=0;j<size-i-1;j++){
			if(arr[j]>arr[j+1]){
				int temp=arr[j];
				arr[j]=arr[j+1];
				arr[j+1]=temp;
			}	
		}
	}
}

void main(){
	int arr[]={-5,-6,3,5,2};
	int size=5;

	sort1(arr,size);
	
	for(int i=0;i<size;i++){
		printf("|%d|",arr[i]);
	}
	printf("\n");
}
