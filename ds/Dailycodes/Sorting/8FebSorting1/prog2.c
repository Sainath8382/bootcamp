//Selection sort: Selection sort is the one which deals with the first place of the array the least value is to be lept at the first position in selection sort

#include<stdio.h>

void selsort(int arr[],int size){
	for(int i=0;i<size-1;i++){
		int minIndex=i;
		for(int j=i+1;j<=size-1;j++){
			if(arr[minIndex]>arr[j]){
				minIndex=j;
			}
		}
		int temp=arr[i];
		arr[i]=arr[minIndex];
		arr[minIndex]=temp;
	}
}

void main(){
	int arr[]={2,-7,-5,3,1,8,5};

	printf("Given array: ");
        int size=sizeof(arr)/sizeof(int);
	for(int i=0;i<size;i++){
		printf("|%d",arr[i]);
	}
	printf("|\n");
	
	selsort(arr,size);
	printf("The sorted array: ");

	for(int i=0;i<size;i++){
		printf("|%d",arr[i]);
	}
	printf("|\n");
}

