//Merge sort :  In merge sort we consider the start ,mid and the end indexes of the array this divides the array elements into single units.
//These single units are then compared after which sorted and then merged into the input array after which we get sorted array as output

#include<stdio.h>

//void merge(int arr[],int start,int mid,int end);
/*
void mergeSort(int arr[],int start,int end){
	
	if(start<end){
		int mid=start+end/2;
		mergeSort(arr,start,mid);
		mergeSort(arr,mid+1,end);
		merge(arr,start,mid,end);
	}	
}
*/
void merge(int arr[],int start,int mid ,int end){
	
	int s1=mid-start+1;
	int s2=end-mid;
	int arr1[s1],arr2[s2];
	
	for(int i=0;i<s1;i++){
		arr1[i]=arr[start+i];
	}
	
	for(int j=0;j<s2;j++){
	       arr2[j]=arr[mid+1+j];
	}

	int itr1=0,itr2=0,itr3=start;
	
	while(itr1<s1 && itr2<s2){
		if(arr1[itr1]<arr2[itr2]){
			arr[itr3]=arr1[itr1];
			itr1++;
		}else{
			arr[itr3]=arr2[itr2];
			itr2++;
		}
		itr3++;
	}
	
	while(itr1<s1){
		arr[itr3]=arr1[itr1];
		itr1++;
		itr3++;
	}

	while(itr2<s2){
		arr[itr3]=arr2[itr2];
		itr2++;
		itr3++;
	}
}

void mergeSort(int arr[],int start,int end){
	if(start<end){
		int mid=(start+end)/2;
		mergeSort(arr,start,mid);
		mergeSort(arr,mid+1,end);
		merge(arr,start,mid,end);
	}
}

void main(){
	int arr[]={5,3,-4,1,7,-6};
	int size=6;
	int start=0,end=size-1;
	mergeSort(arr,start,end);

	printf("The sorted array is \n");
	for(int i=0;i<size;i++){
		printf("%d\t",arr[i]);
	}	
	printf("\n");
}


