//Counting sort is used for some few codes for reducing Time Complexity to O(N). Its real Time Complexity is O(N+k).
//But it is lesser than the Time Complexity of the other previous generic sorting algorithms having Time Complexity as O(N^2).
//For reducing the time complexity it increases the space complexity.
//
//In counting sort none of the elements are compared with one another and the sorting is done. As its Time Complexity is less than others
//  this is an advantage to use counting sort. But it can be used for numbers in a smaller range and the numbers should be nearer to
//  each other.

#include<stdio.h>

void countingsort(int *arr,int N){

	int max = arr[0];
	
	for(int i=1;i<N;i++){

		if(max<arr[i])
			max=*(arr+i);

	}

	int countarr[max+1];

	for(int i=0;i<=max;i++){

		countarr[i]=0;
	}

	for(int i=0;i<N;i++){

		countarr[arr[i]]++;

	}
/*
	printf("The countarray is :\n");
	for(int i=0;i<=max;i++){
		printf("%d\t",*(countarr+i));
	}
	printf("\n");
*/

	for(int i=1;i<=max;i++)
		countarr[i]=countarr[i]+countarr[i-1];

	int output[N];

	for(int i=N-1;i>=0;i--){

		output[countarr[arr[i]]-1] = arr[i];
		countarr[arr[i]]--;
	}

	for(int i=0;i<N;i++)
		*(arr+i)=*(output+i);


}


void main(){

	int arr[]={3,7,2,1,8,2,5,2,7};
	int N=9;

	for(int i=0;i<N;i++)
		printf("%d\t",arr[i]);

	printf("\n");

	printf("The sorted array using counting sort is :\n");
	
	countingsort(arr,N);
	
	for(int i=0;i<N;i++)
		printf("%d\t",arr[i]);

	printf("\n");
	
} 
