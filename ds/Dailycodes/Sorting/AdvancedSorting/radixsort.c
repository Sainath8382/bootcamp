//Radix Sort : Radix sort is used to solve the problems of Counting sort.
//Counting sort required elements in fixed range whereas in radix elements are not in range.
//Counting sort mentions the count of occurences of element where radix considers the count of occurences of
//  digits and sorts accordingly.

#include<stdio.h>

void countingsort(int arr[],int size,int pos){

	int countarr[10] = {0};

	for(int i=0;i<size;i++){
		
		countarr[(arr[i]/pos)%10]++;
	}

	for(int j=1;j<10;j++){

		countarr[j]+=countarr[j-1];
	}

	int output[size];

	for(int i=size-1;i>=0;i--){
		
		output[countarr[(arr[i]/pos)%10]-1] = arr[i];
		
		countarr[(arr[i]/pos)%10]--;
	}

	for(int i=0;i<size;i++){

		arr[i]=output[i];
	}
}


void radix(int *arr,int size){

	int max=arr[0];

	for(int i=0;i<size;i++){

		if(max<arr[i])
			max=arr[i];

	}

	for(int pos = 1;max/pos>0; pos = pos*10){

		countingsort(arr,size,pos);
	}

}

void main(){

	int arr[]={235,5,12,3,470,313,65,20};

	int size= sizeof(arr)/sizeof(arr[0]);

	radix(arr,size);

	printf("The sorted array is \n");

	for(int i=0;i<size;i++){

		printf("%d ",arr[i]);
	}
	
}
	
