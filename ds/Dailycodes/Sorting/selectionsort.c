//Selection sort : Selection sort deals with the first position in the array . The element whose address in minimum is important in selection sort

#include<stdio.h>

void selection(int *arr,int size){
	int start=0,end=size-1;

	for(int i=0;i<size;i++){
		int minindex=i;
		for(int j=i+1;j<size;j++){
			if(arr[j]<arr[minindex])
				minindex=j;
		}
		int temp=arr[i];
		arr[i]=arr[minindex];
		arr[minindex]=temp;
	}
}

void main(){
	int arr[]={5,3,-4,1,7,-6};
	int size=6;

	selection(arr,size);

	printf("The sorted array is \n");
	for(int i=0;i<size;i++){
		printf("%d\t",arr[i]);
	}	
	printf("\n");
}


