//Pre merge sort code 3

#include<stdio.h>
void mergesort(int *arr,int start,int end){
	if(start<end){
		int mid=start+end/2;
		mergesort(arr,start,mid);
		mergesort(arr,mid+1,end);
	}	
}

void main(){
	int arr[]={3,2,-1,7,4,5,8};
	int size=7;

	mergesort(arr,0,size-1);
}
