#include<stdio.h>
#include<stdlib.h>

typedef struct Student{
    int id;
    float ht;
    struct Student *next;
}stud;

void addNode(stud *head){
    stud *newNode=(stud*)malloc(sizeof(stud));
    newNode->id=1;
    newNode->ht=6.2;
    newNode->next=NULL;

    head=newNode;
}

void printLL(stud *head){
    stud *temp=head;

    while(temp!=NULL){
        printf("%d\t",temp->id);
        printf("%f\t",temp->ht);
        temp=temp->next;
    }
}

void main(){
    stud *head=NULL;
    addNode(head);
    printLL(head);
}
