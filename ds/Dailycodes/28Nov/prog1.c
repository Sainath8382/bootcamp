#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct Employee{
    int id;
    char name[10];
    float sal;
    struct Employee *next;
}Emp;

void main(){
    Emp *emp1=(Emp*)malloc(sizeof(Emp));
    Emp *emp2=(Emp*)malloc(sizeof(Emp));
    Emp *emp3=(Emp*)malloc(sizeof(Emp));

    Emp *head=emp1;

    head->id=1;
    strcpy(head->name,"Abhay");
    head->sal=40.55;
    head->next=emp2;

    head->next->id=2;
    strcpy(head->next->name,"Abhi");
    head->next->sal=50.5;
    head->next->next=emp3;

    head->next->next->id=3;
    strcpy(head->next->next->name,"Varun");
    head->next->next->sal=65.5;
    head->next->next->next=NULL;

    printf("%d\t",head->id);
    printf("%s\t",head->name);
    printf("%f\n",head->sal);

    printf("%d\t",head->next->id);
    printf("%s\t",head->next->name);
    printf("%f\n",head->next->sal);

    printf("%d\t",head->next->next->id);
    printf("%s\t",head->next->next->name);
    printf("%f\n",head->next->next->sal);
}
