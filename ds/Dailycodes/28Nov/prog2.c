//printing the elements using function

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct Movie{
    char mname[20];
    int count;
    float price;
    struct Movie *next;
}mv;

void access(mv *ptr){
    printf("%s\t",ptr->mname);
    printf("%d\t",ptr->count);
    printf("%f\t",ptr->price);

    printf("%p\t",ptr->next);
    printf("\n");
}

void main(){
    mv *m1=(mv*)malloc(sizeof(mv));
    mv *m2=(mv*)malloc(sizeof(mv));
    mv *m3=(mv*)malloc(sizeof(mv));
    mv *head=m1;

    strcpy(head->mname,"Drishya");
    head->count=4;
    head->price=450.78;
    head->next=m2;

    strcpy(head->next->mname,"Kgf");
    head->next->count=5;
    head->next->price=500.22;
    head->next->next=m3;

    strcpy(head->next->next->mname,"Kantara");
    head->next->next->count=10;
    head->next->next->price=1100.22;
    head->next->next->next=NULL;

    access(m1);
    access(m2);
    access(m3);
}
