//struct realtime example with malloc

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

struct Software{
    char sname[10];
    int validity;
    float price;
};

void main(){
    struct Software *ptr=(struct Software *)malloc(sizeof(struct Software));
    strcpy(ptr->sname,"Quickheal");
    (*ptr).validity=60;
    (*ptr).price=450.78;

    printf("%s\n",ptr->sname);
    printf("%d\n",ptr->validity);
    printf("%f\n",ptr->price);
}
