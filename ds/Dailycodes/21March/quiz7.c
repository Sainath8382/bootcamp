//My answer is 12.

#include<stdio.h>

int foo(int n,int r){

	if(n>0)
		return (n%r + foo(n/r,r));
	else
		return 0;
}

void main(){

	int ret = foo(345,10);
	printf("%d\n",ret);
}
