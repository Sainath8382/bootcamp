//My answer is x=3,y=4;

#include<stdio.h>

int fun(int x,int y){

	if(y==0)
		return 0;

	return (x + fun(x,y-1));
}

void main(){

	int ret = fun(3,4);
	printf("%d\n",ret);

}
