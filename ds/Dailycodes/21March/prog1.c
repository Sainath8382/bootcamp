//sum of n digits using recursion

#include<stdio.h>

int digitsum(int n){
	
	int sum=0;
	

	if(n>0){
		sum=sum+n;
		return (n%2 + digitsum(n/10));
	}else{
		return 1;
	}

}

void main(){

	int N=123;

	int ret = digitsum(N);
	printf("%d\n",ret);
}
