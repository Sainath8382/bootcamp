//My answer is 9.

#include<stdio.h>

int foo(int n,int r){

	if(n>0)
		return (n%r + foo(n/r,r));
	else
		return 0;
}

void main(){

	int ret = foo(345,8);
	printf("%d\n",ret);
}
