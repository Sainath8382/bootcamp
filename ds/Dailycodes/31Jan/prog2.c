/* Leetcode: IF given number is even then divide by 2, after which if the number is odd then subtract 1 and for even the same count the number of steps till the number becomes zero! 
	eg 14 : even thus divide by 2 : 7
	   7 odd subtract 1 : 6
	   6 even divide by 2 :3 
	   3 odd 3-1=2
	   2 even 2/2=1
	   1 odd 1-1=0
	   steps are 6
*/

//Using recursion

#include<stdio.h>
int countsteps(int num,int count){
	if(num%2==0){
		return countsteps(num%2,count+1);
	}
	if(num%2!=0){
		return countsteps(num-1,count+1);
	}
	return count;
}

void main(){
	int num;
	printf("Enter the number\n");
	scanf("%d",&num);
	int count=0;	
	int steps=countsteps(num,count);
	printf("The number of steps is %d\n",steps);
}
