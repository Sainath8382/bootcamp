//Count no of zeros in a given no using Recursion

#include<stdio.h>
int countzero(int num){
	static int count = 0;
	if(num==0){
		return count;
	}
	if(num%10==0){
	       	count++;
	}
	return countzero(num/10);
}

void main(){
	int num;
	printf("Enter the no\n");
	scanf("%d",&num);

	int count=countzero(num);
	printf("The count is %d\n",count);
}

