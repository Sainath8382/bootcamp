//Searching
//Linear Search
#include<stdio.h>

int ispresent(int *arr,int size,int ele){
	
	for(int i=0;i<size;i++){
		if(arr[i]==ele){
			return i;
		}
	}
	return -1;
}

int lastocc(int *arr,int size,int ele){

	int last=-1;
	for(int i=0;i<size;i++){
		if(arr[i]==ele){
			last=i;
		}
	}
	return last;
}

int secondlastocc(int *arr,int size,int ele){

	int last=-1;
	int seclast=-1;
	for(int i=0;i<size;i++){
		if(arr[i]==ele){
			seclast=last;
			last=i;
		}
	}
	return seclast;
}


void main(){
	int size=8;
	int arr[size];

	printf("Enter array elements:\n");
	for(int i=0;i<size;i++){
		scanf("%d",&arr[i]);
	}

	int ele;
	printf("Enter the element to be checked\n");
	scanf("%d",&ele);
	
	int present=ispresent(arr,size,ele);
	if(present>=0)
		printf("The number given is at index %d\n",present);
	else
		printf("ELEment is not present thus %d\n",present);

	int last=lastocc(arr,8,ele);
	printf("The element last occured at %d\n",last);

	int seclast=secondlastocc(arr,size,ele);
	printf("The element second lastly occured at %d\n",seclast);
}
