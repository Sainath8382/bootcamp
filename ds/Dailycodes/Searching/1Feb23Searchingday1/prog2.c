//Sorted search
//Binary search
//Binary search requires sorted data if not then we have to sort the data anf then perform binary search on it

#include<stdio.h>

int binarysearch(int *arr,int size,int key){

	int start=0,end=size-1,mid;
	while(start<=end){
		mid=start+end/2;

		if(arr[mid]==key){
			return mid;
		}
		if(arr[mid]<key){
			start=mid+1;
		}
		if(arr[mid]>key){
			end=mid-1;
		}
	}
	return -1;
}

void main(){
	int arr[]={1,4,8,12,18,29,32,37,43,50};
	int size=10;

	int index=binarysearch(arr,size,12);
	printf("The number is at %d\n",index);
}
