//Floor value using linear search

#include<stdio.h>
int floorval(int *arr,int size,int key){
	int val=-1;
	for(int i=0;i<size;i++){
		if(arr[i]<=key){
			val=arr[i];
		}
	}
	return val;
}

void main(){
	int arr[]={2,4,5,7,11,17,21};
	int size=7;

	int ret=floorval(arr,size,9);
	printf("The floor value is %d\n",ret);
}
