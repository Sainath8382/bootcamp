//Tower of Hanoi code : This is a game in which there are 3 towers A,B and C the Tower 'A' contains disks which are on it in increasing 
//	order from top till the bottom. The goal is to shift all the disks from "A -> C" with the help of 'B' in the same order as they
//	are present on tower 'A' .
//	The constraint is that larger disk should not be present above the smaller disk at any instance. Print all the steps associated
//	for performing the operation.

#include<stdio.h>

void TOH(int N,char A,char B,char C){

	if(N>0){

		TOH(N-1,A,C,B);
		printf("%c -> %c\n",A,C);
		TOH(N-1,B,A,C);
	}

}

void main(){

	int N;
	printf("Enter the number of disks for the tower\n");
	scanf("%d",&N);
	
	char A='A',B='B',C='C';
	TOH(N,A,B,C);
}
