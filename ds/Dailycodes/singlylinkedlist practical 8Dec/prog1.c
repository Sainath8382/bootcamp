#include<stdio.h>
#include<stdlib.h>
typedef struct Node{
    int data;
    struct Node *next;
}Node;

Node *head=NULL;

struct Node* createNode(){
    struct Node *newNode=(struct Node*)malloc(sizeof(struct Node));
    int data;
    printf("Enter data:\n");
    scanf("%d",&newNode->data);
    newNode->next=NULL;

    return newNode;
}

void addNode(){
    struct Node *newNode=createNode();
    if(head==NULL){
        head=newNode;
    }else{
        struct Node *temp=head;
        while(temp->next!=NULL){
            temp=temp->next;
        }
        temp->next=newNode;
    }
}

void addFirst(){
    struct Node *newNode=createNode();
    if(head==NULL){
        head=newNode;
    }else{
        newNode->next=head;
        head=newNode;
    }
}

void addLast(){
    addNode();
}

int countnode(){
    struct Node *temp=head;
    int count=0;

    while(temp!=NULL){
        count++;
        temp=temp->next;
    }
    printf("count=%d\n",count);
    return count;
}

void addatPos(int pos){
    int c=countnode();
    if(pos<=0 || pos>=c+2){
        printf("Invalid node operation\n");
    }else{
        if(pos==1){
            addFirst();
        }else if(pos==c+1){
            addLast();
        }else{
            struct Node *newNode=createNode();
            struct Node *temp=head;

            while(pos-2){
                temp=temp->next;
                pos--;
            }
            newNode->next=temp->next;
            temp->next=newNode;
        }
    }
}

void delFirst(){
    if(head==NULL){
        printf("Invalid operation\n");
    }else if(head->next=NULL){
        free(head);
        head=NULL;
    }else{
        Node *temp=head;
        head=temp->next;
        free(temp);
    }
}

void delLast(){
    Node *temp=head;
    if(head->next==NULL){
        free(temp);
        head=NULL;
    }else{
        Node *temp=head;
        while(temp->next->next!=NULL){
            temp=temp->next;
        }
        free(temp->next);
        temp->next=NULL;
    }
}

int delatPos(int pos){
    int count=countnode();
    if(pos<=0 || pos>count){
        printf("Invalid operation\n");
        return -1;
    }else{
        if(pos==1){
            delFirst();
        }else if(pos==count){
            delLast();
        }else{
            Node *temp=head;

            while(pos-2){
                temp=temp->next;
                pos--;
            }
            Node *temp2=temp->next;
            temp->next=temp2->next;
            free(temp2);
        }
    }
    return 0;
}

void occurence(){
    Node *temp=head;
    int ele;
    printf("Enter the element:\n");
    scanf("%d",&ele);
    int count=0;

    while(temp!=NULL){
        count++;
        if(temp->data==ele){
            printf("%d\n",count);
            break;
        }
        temp=temp->next;
    }
}

void printLL(){
    struct Node *temp=head;
    while(temp->next!=NULL){
        printf("|%d|",temp->data);
        temp=temp->next;
    }
    printf("|%d|",temp->data);
}

void main(){
    int nodecount;
    printf("ENter the count of nodes:\n");
    scanf("%d",&nodecount);

    for(int i=0;i<nodecount;i++){
        addNode();
    }

    printLL();
    occurence();
}
