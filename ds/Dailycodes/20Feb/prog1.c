//user array if a number is found is greater than other nos increase the count

#include<stdio.h>
int count(int *arr,int size){
	int count=0;
	for(int i=0;i<size;i++){
		int val=arr[i];
		for(int j=0;j<size;j++){
			if(val<arr[j]){
				count++;
				break;
			}
		}
	}

	return count;
}

void main(){
	int arr[]={7,3,-2,1,9};
	int ret=count(arr,8);
	printf("The count is %d\n",ret);
}
