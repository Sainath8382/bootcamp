//optimized way for prog1.c

#include<stdio.h>
int count(int arr[],int size){
	int count=0;
	int max=arr[0];
	for(int i=0;i<size;i++){
		if(arr[i]>max)
			max=arr[i];
	}

	for(int i=0;i<size;i++){
		if(max>arr[i])
			count++;
	}
	return count;
}

void main(){
	int arr[]={7,3,-2,1,9};
	int N=5;

	int ret=count(arr,N);
	printf("The count is %d\n",ret);
}
