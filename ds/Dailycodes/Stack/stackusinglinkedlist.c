#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>

struct Node{
    int data;
    struct Node *next;
};

int countNode=0;
struct Node *head=NULL;
int top=0;

struct Node *createNode(){
    struct Node *newNode=(struct Node*)malloc(sizeof(struct Node));
    printf("ENter data:");
    scanf("%d",newNode->data);
    newNode->next=NULL;

    return newNode;
}

struct Node* push(){
    struct Node* newNode=createNode();
    if(head==NULL){
        head=newNode;
        top++;
    }else{
        struct Node *temp=head;
        while(temp->next!=NULL){
            temp=temp->next;
        }
        temp->next=newNode;
        top=newNode;
    }
}

int elecount(){
    struct Node *temp=head;
    int count=0;
    while(temp->next!=NULL){
        count++;
        temp=temp->next;
    }
    return count;
}

bool isFull(){
    if(elecount()==countNode){
        return true;
    }else{
        return false;
    }
}
/*
int push(){
    if(isFull()){
        return -1;
    }else{
        addNode();
        return 0;
    }
}
*/
bool isEmpty(){
    if(elecount==0){
        return true;
    }else{
        return false;
    }
}

int flag=0;

int pop(){
    if(isEmpty()){
        return -1;
    }else{
        flag=1;
        top=head;
        int data=top->data;

        while(top->next->next!=NULL){
            top=top->next;
        }
        free(top->next);
        top->next=NULL;
        return data;
    }
}

void main(){
    printf("Enter size of stack: ");
    scanf("%d",&countNode);
    char choice;

    do{
        printf("1.push\n");
        printf("2.pop\n");
        int ch;
        printf("Enter your choice:\n");
        scanf("%d",&ch);

        switch(ch){
            case 1:
                push();
                break;

            case 2:
                int data;
                data=pop();
                printf("%d popped\n",data);
        }
    }while(choice=='y'  || choice=='Y');
}
