//Implementing priority queue using linked list part 1 (creating the basic code for priority queue)

#include<stdio.h>
#include<stdlib.h>

struct Node{
	int data;
	struct Node *next;
};

struct Node *createNode(){
	struct Node *newNode=(struct Node *)malloc(sizeof(struct Node));
	printf("Enter queue data: ");
	scanf("%d",&newNode->data);
	newNode->next=NULL;

	return newNode;
}

struct Node *head=NULL;

void enqueue(){
	struct Node *newNode=createNode();
	if(head==NULL){
		head=newNode;
	}else{
		struct Node *temp=head;
		struct Node *prev=NULL;
		while(temp->next!=NULL){
			if(newNode->data>=temp->data){
				break;
			}
			prev=temp;
			temp=temp->next;
		}
		if(prev==NULL){
			head=newNode;
			newNode->next=temp;
		}else{
			prev->next=newNode;
			newNode->next=temp;
		}
	}
}

int flag=0;

int dequeue(){
	if(head==NULL){
		flag=0;
		return -1;
	}else{
		flag=1;
		struct Node *temp=head;
		int val=temp->data;
		head=temp->next;
		free(temp);
		return val;
	}
}

void printqueue(){
	if(head==NULL){
		printf("Queue is empty\n");
		//return -1;
	}else{
		struct Node *temp=head;
		while(temp->next!=NULL){
			printf("%d ",temp->data);
			temp=temp->next;
		}
		printf("%d\n",temp->data);
//		return 0;
	}
}

void main(){
	char choice;
	do{
		printf("1.Enqueue()\n");
		printf("2.Dequeue()\n");
		printf("3.printqueue()\n");

		int ch;
		printf("enter choice: ");
		scanf("%d",&ch);
		switch(ch){
			case 1:
				enqueue();
				break;

			case 2:
				{	
					int val=dequeue();
					if(flag==0){
						printf("Queue underflow\n");
					}else{
						printf("%d dequed\n",val);
					}
				}
				break;

			case 3:
				printqueue();
				break;

			default:
				printf("Wrong choice\n");
			break;	
		}
		getchar();
		printf("COntinue(y/n): ");
		scanf("%c",&choice);
	}while(choice == 'Y' || choice == 'y');
}

/*
void main(){

	int count;
	printf("enter queue elements: ");
	scanf("%d",&count);

	for(int i=1;i<=count;i++){
		enqueue();
	}
	
	printqueue();
	int ret=dequeue();
	if(flag==0)
		printf("Queue underflow\n
*/


































