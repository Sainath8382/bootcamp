// priority queue implementation

#include<stdio.h>
#include<stdlib.h>

typedef struct Node{
	int data;
	int pri;
	struct Node *next;
}node;

node *createNode(){
	node *newNode=(node *)malloc(sizeof(node));
	int ch=1;
	do{
		printf("Enter priority: ");
		scanf("%d",&newNode->pri);
		if(newNode->pri<0 ||newNode->pri>5)
			printf("Invalid priority\n");
		else
			ch=0;		

	}while(ch);

	printf("enter data: ");
	scanf("%d",&newNode->data);
	newNode->next=NULL;

	return newNode;
}

node *head=NULL;
int flag=0;

void enqueue(){
	node *newNode=createNode();
	if(head==NULL){
		head=newNode;
	}else{
		struct Node *temp=head;
		struct Node *prev=NULL;

		while(temp->next!=NULL){
           		if(newNode->data>=temp->data)
				break;
			prev=temp;
			temp=temp->next;
		}
		if(prev==NULL){
			head=newNode;
			newNode->next=temp;
		}else{
			newNode->next=temp;
			prev->next=newNode;
		}
	}
}

int dequeue(){
	if(head==NULL){
		flag=0;
		return -1;
	}else{
		flag=1;
		node *temp=head;
		int val=temp->data;
		head=temp->next;
		free(temp);

		return val;
	}
}

void printqueue(){
	if(head==NULL){
		printf("Queue is empty\n");
	}else{
		node *temp=head;
		while(temp->next!=NULL){
			printf("%d ",temp->data);
			temp=temp->next;
		}
		printf("%d\n",temp->data);
	}
}

void main(){
	int count;
	printf("Enter queue count:\n");
	scanf("%d",&count);

	for(int i=1;i<=count;i++){
		enqueue();
	}
	printqueue();

	int ret=dequeue();
	if(flag==0){
		printf("Underflow\n");
	}else{
		printf("%d dequeued\n",ret);
	}
}



















































































