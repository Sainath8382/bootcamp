//Here given a 1-D array and another 2-D array arr2 the start of range will be arr2[i][0],end will be arr2[i][1] to return the array
//such that it contains the count of even elements in the range.

#include<stdio.h>

void evenrange(int arr1[],int rows,int rows2,int cols2,int B[rows2][cols2]){
	
	for(int i=0;i<rows;i++){

		int start = B[i][0],end = B[i][1];
		int count=0;

		for(int j=start;j<=end;j++){
			if(arr1[j]%2==0)
				count++;
		}
		printf("%d\t",count);
	}
	printf("\n");

}

void main(){
	int arr[]={1,2,3,4,5};
	int N=5;
	int arr2[][2]={{0,2},{2,4},{1,4}};
	int rows2=2,cols2=2;
	evenrange(arr,N,rows2,cols2,arr2);
}

