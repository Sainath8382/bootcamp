//Rotate the array times the given value

#include<stdio.h>

void rotate(int arr[],int N,int R){
	for(int i=0;i<R;i++){
		int temp=arr[N-1];
		for(int j=N-1;j>0;j--){
			arr[j]=arr[j-1];
		}
		arr[0]=temp;
	}

	for(int i=0;i<N;i++)
		printf("%d\t",arr[i]);

	printf("\n");
	
}

void main(){
	int arr[]={1,2,3,4,5};
	int N=5;
	int R=3;

	rotate(arr,N,R);
}
