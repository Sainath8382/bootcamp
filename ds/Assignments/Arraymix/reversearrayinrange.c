//A range is given reverse the array in that range

#include<stdio.h>

void revrange(int arr[],int size,int B,int C){
	for(int i=B;i<=C;i++){
		int temp=arr[i];
		arr[i]=arr[C];
		arr[C]=temp;
		C--;
	}

	for(int i=0;i<size;i++){
		printf("%d\n",arr[i]);
	}
}

void main(){
	int arr[]={1,2,3,4,5};
	int N=5;
	int B=1;
	int C=3;

	revrange(arr,N,B,C);

}
