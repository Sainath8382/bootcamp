//Reverse the entire array and store the reversed in another new array 

#include<stdio.h>

void reverse(int *arr,int N){
	int i=0;
	int j=N-1;

	int arr1[N];
	while(j>=0){
		arr1[i]=arr[j];
		j--;
		i++;
	}

	for(int i=0;i<N;i++){
		printf("%d\t",arr1[i]);
	}
	printf("\n");
}

void main(){
	int arr[]={1,1,10,23};
	int N=4;

	reverse(arr,N);
}
