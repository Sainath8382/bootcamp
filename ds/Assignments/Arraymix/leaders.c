//Leaders in array

#include<stdio.h>

void leaders(int arr[],int size){
		
	for(int i=0;i<size;i++){
		int max=arr[i];	
		for(int j=i+1;j<size;j++){
			if(arr[j]>max)
				max=arr[j];
		}
		printf("%d\t",max);
	
	}
	printf("\n");
}


void leaders2(int arr[],int size){
	
	int max=arr[0];
	for(int i=0;i<size;i++){
		for(int j=i+1;j<size;j++){
			if(arr[j]>max)
				max=arr[j];
		}
	}
	printf("%d",max);
	
	printf("\n");
}

void main(){
	int arr[]={16,17,4,3,5,2};
	int N=6;

	leaders(arr,N);
}
