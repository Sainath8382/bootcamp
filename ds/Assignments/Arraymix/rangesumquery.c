//here 1-D array is given and also a 2-D array is given. The inputs in the 2-D array will be the start and end values for the sum in range.

#include<stdio.h>

void rangesum(int *arr1,int size,int M,int B[2][2]){
	int range[M];
	for(int i=0;i<M;i++){
		
		int start=B[i][0];
		int end=B[i][1];
		int sum=0;
		
		for(int j=start;j<=end;j++){
			sum=sum+arr1[j];
		}
		
		range[i]=sum;
		printf("%d\n",range[i]);
	}
	
}

void main(){
	int arr[]={1,2,3,4,5};
	int N=5;
	int M=2;

	int B[2][2]={{0,3},{1,2}};

	rangesum(arr,N,M,B);
}
