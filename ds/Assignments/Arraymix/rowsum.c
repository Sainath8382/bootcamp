//row sum : take the sum of all the rows and store it into the 1d array 

#include<stdio.h>
int rowsum(int rows,int cols,int arr[rows][cols]){
	
	int rsum[rows];
	for(int i=0;i<rows;i++){
		int sum=0;
		for(int j=0;j<cols;j++){
			sum=sum+arr[i][j];
		}
		rsum[i]=sum;
	}
	return rsum;
}

void main(){
	int rows=3,cols=4;
	int arr[rows][cols]={{1,2,3,4},{5,6,7,8},{4,2,3,4}};

	int *sum[]=rowsum(rows,cols,arr);
	for(int i=0;i<rows;i++){
	printf("%d\n",sum[i]);
	}
}	
