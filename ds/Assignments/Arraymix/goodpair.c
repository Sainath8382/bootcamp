//Good pair : here i!=j && a[i]+a[j]=B
//arr[]={1,2,3,4} B=7
//op is 1 as 3+4=7 is a good pair

#include<stdio.h>

int goodpair(int arr[],int size,int B){
	for(int i=0;i<size;i++){
		for(int j=0;j<size;j++){
			if(i!=j && arr[i]+arr[j]==B)
				return 1;
		}
	}
	return 0;
}

void main(){
	int arr[]={1,2,4};
	int B=4;
	int N=3;

	int ret=goodpair(arr,N,B);
	printf("%d\n",ret);
}
