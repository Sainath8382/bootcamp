//Column sum of 2d array and store it in a 1D array

#include<stdio.h>

void column(int rows,int cols,int arr[rows][cols]){
	int sumarr[cols];

	for(int i=0;i<cols;i++){
		int sum=0;
		for(int j=0;j<rows;j++){
			sum=sum+arr[j][i];
		}
		sumarr[i]=sum;
	}
	
	for(int i=0;i<cols;i++){
		printf("%d\t",sumarr[i]);
	}
	printf("\n");
}

void main(){
	int rows=3,cols=4;
	int arr[3][4]={{1,2,3,4},{5,6,7,8},{9,2,3,4}};
	column(rows,cols,arr);
}
