#include<stdio.h>

int minor(int rows,int cols,int arr[rows][cols]){
	
	int sum=0;
	for(int i=0;i<rows;i++){
		for(int j=0;j<cols;j++){
			if(i+j==2)
				sum=sum+arr[i][j];
			
		}
	}
	return sum;
}

void main(){
	int arr[][3]={{1,2,3},{4,5,6},{7,8,9}};
	int rows=3,cols=3;

	int sum=minor(rows,cols,arr);
	printf("%d\n",sum);
}
