//Count the no of integers which have atleast 1 element greater than itself

#include<stdio.h>

void main(){
	int arr[]={5,5,3};

	int N=3;
	int count=0;

	for(int i=0;i<N-1;i++){
		if(arr[i+1]>arr[i])
			count++;
		else if(arr[i]>arr[i+1])
			count++;
	}
	printf("%d\n",count);
}
