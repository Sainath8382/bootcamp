//Row to column zero if arr[i][j]=0 then for that entire row and column make all the elements as zero

#include<stdio.h>

void zero(int rows,int cols,int arr[rows][cols]){

	for(int i=0;i<rows;i++){
		for(int j=0;j<cols;j++){

			if(arr[i][j]==0){

				for(int k=0;k<rows;k++){

					arr[i][k]=0;
				}

				for(int m=0;m<cols;m++){
					arr[m][j]=0;
				}
			}
		}
	}

	printf("The converted array is\n");
	for(int i=0;i<rows;i++){
		for(int j=0;j<cols;j++){
			printf("%d\t",arr[i][j]);
		}
		printf("\n");
	}
}

void main(){
	int arr[][4]={{1,2,3,4},{5,6,7,0},{9,2,0,4}};
	int rows=3,cols=4;

	zero(rows,cols,arr);
}
