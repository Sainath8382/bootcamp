//Equilibrium index: Equilibrium index is the index where the sum of elements before the equilibrium index is equal to the sum of elements to the right side 
//of the equilibrium index

#include<stdio.h>

int equilibrium(int arr[],int N){

	for(int i=0;i<N;i++){
		int sum1=0,sum2=0,index=N;
		
		for(int j=i-1;j>=0;j--){
			sum1=sum1+arr[j];
		}

		for(int k=i+1;k<N;k++){
			sum2=sum2+arr[k];
		}

		if(sum1==sum2 && i<index){
			index=i;
			return index;
		}
	}

		return 0;
}

void main(){

	int arr[]={-7,1,5,2,-4,3,0};
	int N=7;

	int index = equilibrium(arr,N);
	printf("The equilibrium index is :%d\n",index);
}

