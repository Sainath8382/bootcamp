//Special subsequences AG : return the count of how many subsequences of AG are present in the given 1-D array

#include<stdio.h>

int subseq(char *arr,int size){
	int count=0;

	for(int i=0;i<size;i++){
		
		for(int j=i+1;j<size;j++){
			if(arr[i]=='A' && arr[j]=='G')
					count++;
		}
	}
	return count;
}

void main(){
	char carr[]={'A','B','C','G','A','G'};
	int N=6;

	int count=subseq(carr,N);
	printf("The count of subsequences AG is : %d\n",count);
}

