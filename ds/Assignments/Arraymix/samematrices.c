#include<stdio.h>

int same(int rows,int cols,int arr1[rows][cols],int arr2[rows][cols]){

	for(int i=0;i<rows;i++){
		for(int j=0;j<cols;j++){
			if(arr1[i][j]!=arr2[i][j]){
				return 0;
			}
		}
	}
	return 1;
}

void main(){
	int arr1[3][3]={{1,2,3},{10,11,12},{20,21,22}};
	int arr2[3][3]={{1,2,3},{10,45,12},{20,21,22}};
	int rows=3,cols=3;

	int ret=same(rows,cols,arr1,arr2);
	if(ret==1)
		printf("Both are same\n");
	else
		printf("Both are not same\n");
}

