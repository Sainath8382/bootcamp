//concat n nodes leetcode problem

#include<stdio.h>
#include<stdlib.h>

struct Node{
    int data;
    struct Node *next;
};

struct Node *createNode(){
    struct Node *newNode=(struct Node *)malloc(sizeof(struct Node));

    printf("Enter data: ");
    scanf("%d",&newNode->data);

    newNode->next=NULL;
    return newNode;
}

struct Node *head1=NULL;
struct Node *head2=NULL;

void addNode(struct Node **head){
    struct Node *newNode=createNode();

    if(*head==NULL){
        *head=newNode;
    }else{
        struct Node *temp=*head;
        while(temp->next!=NULL){
            temp=temp->next;
        }
        temp->next=newNode;
    }
    //return head;
}

int printSLL(){

    if(head1==NULL || head2==NULL){
        printf("Linked List empty\n");
        return -1;
    }else{
        struct Node *temp=head1;
        while(temp->next!=NULL){
            printf("|%d|->",temp->data);
            temp=temp->next;
        }
        printf("|%d|\n",temp->data);
        return 0;
    }
}

int countnode(){
    struct Node *temp=head2;
    int count;
    while(temp!=NULL){
        count++;
        temp=temp->next;
    }
    return count;
}

void concatLL(){
    struct Node *temp1=head1;
    while(temp1->next!=NULL){
        temp1=temp1->next;
    }

    struct Node *temp2=head2;
    temp1->next=temp2;
}

/*
void concatNLL(int num){
    struct Node *temp1=head1;
    struct Node *temp2=head2;
    while(temp1->next!=NULL)
        temp1=temp1->next;
    int count=countnode();
    int val=count-num;

    while(val){
        temp2=temp2->next;
        val--;
    }
    temp1->next=temp2;
}
*/

int concatNLL(int num){

    if(head1==NULL || head2==NULL){
        printf("First fill the linked lists with data!");
        return -1;
    }else{
        if(num<=0 || num>countnode()){
            printf("Invalid operation\n");
            return -2;
        }else{
            int pos = countnode() - num;
            struct Node *temp1=head1;

            while(temp1->next!=NULL){
                temp1=temp1->next;
            }

            struct Node *temp2= head2;
            while(pos){
                temp2 = temp2->next;
                pos--;
            }
            temp1->next = temp2;
        }
    }
    return 0;
}

void main(){
    int nodecount;
    printf("ENter the no of nodes:LINKED LIST 1\n");
    scanf("%d",&nodecount);

    for(int i=1;i<=nodecount;i++){
        addNode(&head1);
    }

    printf("ENter the no of nodes:LINKED LIST 2\n");
    scanf("%d",&nodecount);
    for(int i=1;i<=nodecount;i++){
        addNode(&head2);
    }

    printSLL(head1);
    printSLL(head2);

    concatLL(head1,head2);
    printSLL(head1);

    int num;
    printf("Enter the number of nodes for concat:\n");
    scanf("%d",&num);

    concatNLL(num);
    printSLL();
}
