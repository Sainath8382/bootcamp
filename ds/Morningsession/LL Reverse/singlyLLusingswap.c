//Linked List Reverse (using swap)

#include<stdio.h>
#include<stdlib.h>
struct Demo{
    int data;
    struct Demo *next;
};

struct Demo *head=NULL;

struct Demo* createNode(){
    struct Demo *newNode=(struct Demo*)malloc(sizeof(struct Demo));
    int data;
    printf("Enter data:\n");
    scanf("%d",&newNode->data);
    newNode->next=NULL;

    return newNode;
}

void addNode(){
    struct Demo *newNode=createNode();
    if(head==NULL){
        head=newNode;
    }else{
        struct Demo *temp=head;
        while(temp->next!=NULL){
            temp=temp->next;
        }
        temp->next=newNode;
    }
}

void addFirst(){
    struct Demo *newNode=createNode();
    if(head==NULL){
        head=newNode;
    }else{
        newNode->next=head;
        head=newNode;
    }
}

void addLast(){
    addNode();
}

int nodecount(){
    struct Demo *temp=head;
    int count=0;

    while(temp!=NULL){
        count++;
        temp=temp->next;
    }
    //printf("count=%d\n",count);
    return count;
}

void addatPos(int pos){
    struct Demo *newNode=createNode();
    struct Demo *temp=head;
    while(pos-2){
        temp=temp->next;
        pos--;
    }
    newNode->next=temp->next;
    temp->next=newNode;
}

void printLL(){
    struct Demo *temp=head;
    while(temp!=NULL){
        printf("|%d|->",temp->data);
        temp=temp->next;
    }
}

void delFirst(){
    struct Demo *temp=head;
    head=temp->next;
    free(temp);
}

void delLast(){
    struct Demo *temp=head;
    while(temp->next->next!=NULL){
        temp=temp->next;
    }
    free(temp->next);
    temp->next=NULL;
}

int revLL(){

    if(head==NULL){
        printf("Linked List empty\n");
        return -1;
    }else{
             int count=nodecount();
            int iter=0,cnt=count/2;
        struct Demo *temp1=head;
        struct Demo *temp2=head;

        while(cnt){
            while(nodecount()-iter){
                temp2=temp2->next;
            }
            int temp;
            temp=temp2->data;
            temp2->data=temp1->data;
            temp1->data=temp;

            temp1=temp1->next;
            cnt--;
            iter++;
            }
            return 0;
        }
}

void main(){
    char choice;

    do{
        printf("1.addNode\n");
        printf("2.addFirst\n");
        printf("3.addLast\n");
        printf("4.count\n");
        printf("5.addatPos\n");
        printf("6.printLL\n");
        printf("7.deleteFirst\n");
        printf("8.deleteLast\n");
        printf("9.revLL\n");

        int ch;
        printf("ENter your choice:\n");
        scanf("%d",&ch);

        switch(ch){
            case 1:
                addNode();
                break;

            case 2:
                addFirst();
                break;

            case 3:
                addLast();
                break;

            case 4:
                nodecount();
                break;

            case 5:{
                int pos;
                printf("Enter the pos:\n");
                scanf("%d",&pos);

                addatPos(pos);
                }
                break;

            case 6:
                printLL();
                break;

            case 7:
                delFirst();
                break;

            case 8:
                delLast();
                break;

            case 9:
                revLL();
                break;
        }
        getchar();
        printf("Do you want to continue:\n");
        scanf("%c",&choice);
    }while(choice=='y' || choice=='Y');
}
