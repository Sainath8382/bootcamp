#include<stdio.h>
#include<stdlib.h>
struct Node{
    int data;
    struct Node *next;
};

struct Node *createNode(){
    struct Node *newNode=(struct Node*)malloc(sizeof(struct Node));
    printf("ENter data:");
    scanf("%d",&newNode->data);
    newNode->next=NULL;

    return newNode;
}

struct Node *head=NULL;

void addNode(){
    struct Node *newNode=createNode();
    if(head==NULL){
        head=newNode;
        newNode->next=head;
    }else{
        struct Node *temp=head;
        while(temp->next!=head){
            temp=temp->next;
        }
        newNode->next=head;
        temp->next=newNode;
    }
}

int printLL(){
    if(head==NULL){
        printf("LL is empty\n");
        return -1;
    }else{
        struct Node *temp=head;
        while(temp->next!=head){
            printf("|%d|->",temp->data);
            temp=temp->next;
        }
        printf("|%d|\n",temp->data);
        return 0;
    }
}

int revSCLL(){/*
    if(head==NULL){
        printf("LL is empty\n");
        return -1;
    }else{*/
        struct Node *temph=head;
        struct Node *temp1=NULL;
        struct Node *temp2=NULL;

        while(temp1!=head){
            temp1=temph->next;
            temph->next=temp2;
            temp2=temph;
            temph=temp1;
        }
        head->next=temp2;
        head=temp2;

}

void main(){
    int  nodecount;
    printf("Enter nodecount:");
    scanf("%d",&nodecount);

    if(nodecount<0){
        printf("Invalid nodecount\n");
    }else{
        for(int i=0;i<nodecount;i++){
            addNode();
        }
        printLL();
        revSCLL();
        printLL();
    }
}
