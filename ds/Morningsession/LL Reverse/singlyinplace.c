//singly inplace reverse

#include<stdio.h>
#include<stdlib.h>
struct Node{
    int data;
    struct Node *next;
};

struct Node *createNode(){
    struct Node *newNode=(struct Node*)malloc(sizeof(struct Node));
    printf("Enter data:");
    scanf("%d",&newNode->data);
    newNode->next=NULL;

    return newNode;
}

struct Node *head=NULL;

void addNode(){
    struct Node *newNode=createNode();
    if(head==NULL){
        head=newNode;
    }else{
        struct Node* temp=head;
        while(temp->next!=NULL){
            temp=temp->next;
        }
        temp->next=newNode;
    }
}

int printSLL(){
    if(head==NULL){
        printf("Linked List is empty\n");
        return -1;
    }else{
        struct Node *temp=head;
        while(temp->next!=NULL){
            printf("|%d|->",temp->data);
            temp=temp->next;
        }
        printf("|%d|\n",temp->data);
        return 0;
    }
}


int revSLL(){
    if(head==NULL){
        printf("LL empty\n");
        return -1;
    }else if(head->next==NULL){
        printf("Only one node cannot reverse\n");
        return -1;
    }else{
        struct Node *temp1=NULL;
        struct Node *temp2=NULL;
        while(head!=NULL){
            temp2=head->next;
            head->next=temp1;
            temp1=head;
            head=temp2;
        }
        head=temp1;
        return 0;
    }
}

void main(){
    int nodecount;
    printf("Enter the node count:");
    scanf("%d",&nodecount);

    if(nodecount<0){
        printf("ENter valid node count\n");
    }else{
        for(int i=1;i<=nodecount;i++){
            addNode();
        }
            printSLL();
            revSLL();
        printSLL();
    }
}
