#include<stdio.h>
#include<stdlib.h>
struct Node{
    struct Node *prev;
    int data;
    struct Node *next;
};

struct Node *createNode(){
    struct Node *newNode=(struct Node*)malloc(sizeof(struct Node));
    newNode->prev=NULL;
    printf("ENter data:");
    scanf("%d",&newNode->data);
    newNode->next=NULL;

    return newNode;
}

struct Node *head=NULL;

void addNode(){
    struct Node *newNode=createNode();

    if(head==NULL){
        head=newNode;
    }else{
        struct Node *temp=head;
        while(temp->next!=NULL){
            temp=temp->next;
        }
        temp->next=newNode;
        newNode->prev=temp;
    }
}

int printLL(){
    if(head==NULL){
        printf("Linked List Empty\n");
        return -1;
    }else{
        struct Node *temp=head;
        while(temp->next!=NULL){
            printf("|%d|->",temp->data);
            temp=temp->next;
        }
        printf("|%d|\n",temp->data);
        return 0;
    }
}

int revDLL(){
    if(head==NULL){
        printf("Linked List is EMPTY!\n");
        return -1;
    }else{
            struct Node *temp=NULL;
            while(head!=NULL){
                head->prev=head->next;
                head->next=temp;
                temp=head;
                head=head->prev;
            }
            head=temp;
            return 0;
    }
}

void main(){
    int nodecount;
    printf("Enter node count:");
    scanf("%d",&nodecount);

    for(int i=1;i<=nodecount;i++){
        addNode();
    }

    printLL();
    revDLL();
    printLL();
}
