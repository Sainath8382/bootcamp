//Doubly circular linked list (in place reverse)

#include<stdio.h>
#include<stdlib.h>

struct Node{
    struct Node *prev;
    int data;
    struct Node *next;
};

struct Node *createNode(){
    struct Node *newNode=(struct Node*)malloc(sizeof(struct Node));
    newNode->prev=NULL;
    printf("Enter data:");
    scanf("%d",&newNode->data);
    newNode->next=NULL;

    return newNode;
}

struct Node *head=NULL;

void addNode(){
    struct Node *newNode=createNode();
    if(head==NULL){
        head=newNode;
        newNode->prev=head;
        newNode->next=head;
    }else{
        head->prev->next=newNode;
        newNode->prev=head->prev;

        newNode->next=head;
        head->prev=newNode;
    }

}

int printDLL(){
    if(head==NULL){
        printf("LL empty\n");
        return -1;
    }else{
        struct Node *temp=head;
        while(temp->next!=head){
            printf("|%d|->",temp->data);
            temp=temp->next;
        }
        printf("|%d|\n",temp->data);
        return 0;
    }
}



void addFirst(){
    struct Node *newNode=createNode();
    if(head==NULL){
        head=newNode;
        newNode->prev=head;
        newNode->next=head;
    }else{
        newNode->next=head;
        head->prev->next=newNode;
        newNode->prev=head->prev;
        head->prev=newNode;
        head=newNode;
    }
    printDLL();
}

void addLast(){
    addNode();
    printDLL();
}

int nodecount(){
    int count=0;
    struct Node *temp=head;
    while(temp->next!=head){
        count++;
        temp=temp->next;
    }

    return count+1;
}

int addatPos(int pos){
    int count=nodecount();

    if(pos<=0 || pos>count+1){
        printf("Invalid operation\n");
        return -1;
    }else{
        if(pos==1){
            addFirst();
        }else if(pos==count+1){
            addNode();
        }else{
            struct Node *newNode=createNode();
            struct Node *temp=head;
            while(pos-2){
                temp=temp->next;
                pos--;
            }
            newNode->next=temp->next;
            newNode->prev=temp;
            temp->next=newNode;
            newNode->next->prev=newNode;
        }
        //return 0;
    }
    printDLL();
    return 0;
}

int delFirst(){
    if(head==NULL){
        printf("LL empty\n");
        return -1;
    }else{
        if(head->next==head){
            free(head);
            head=NULL;
        }else{
            struct Node *temp=head;
            while(temp->next!=head)
                temp=temp->next;

            temp->next=head->next;
            free(head);
            head=temp->next;
            head->prev=temp;
        }
    }
    printDLL();
    return 0;
}

int delLast(){
    if(head==NULL){
        printf("LL empty\n");
        return -1;
    }else{
        if(head->next==head){
            free(head);
            head=NULL;
        }else{
            struct Node *temp=head;
            while(temp->next!=head){
                temp=temp->next;
            }
            head->prev=temp->prev;
            head->prev->next=head;
            free(temp);
        }
        return 0;
    }
    printDLL();
}

int delatPos(int pos){
    int count=nodecount();
    if(pos <=0 || pos >count ){
        printf("Invalid operation\n");
        return -1;
    }else{
        if(pos==1){
            delFirst();
        }else if(pos==count){
            delLast();
        }else{
            struct Node *temp=head;
            while(pos-2){
                temp=temp->next;
                pos--;
            }
            temp->next=temp->next->next;
            free(temp->next->prev);
            temp->next->prev=temp;
        }
        return 0;
    }
    printDLL();
}

int revDLL(){
    if(head==NULL){
        printf("LL empty\n");
        return -1;
    }else{
        if(head->next==head){
            printf("Only one node present\n");
        }else{
            struct Node *temp=NULL;
            while(head!=NULL){
                head->prev=head->next;
                head->next=temp;

                head=head->prev;
                if(head!=NULL){
                    temp=head->prev;
                }
            }
            head=temp->prev;
        }
        //return 0;
    }
}

void main(){
    printf("Welcome to Doubly Circular Linked List !!\n");
    char choice;

    do{
        int ch;
        printf("1.AddNode\n");
        printf("2.addFirst\n");
        printf("3.Addatpos\n");
        printf("4.Addlast\n");
        printf("5.delFirst\n");
        printf("6.delLast\n");
        printf("7.delatPos\n");
        printf("8.printDLL\n");
        printf("9.RevDLL\n");

        printf("enter your choice: ");
        scanf("%d",&ch);

        switch(ch){
        case 1:
            addNode();
            break;

        case 2:
            addFirst();
            break;

        case 3:
            {
            int pos;
            printf("enter your position: ");
            scanf("%d",&pos);
            addatPos(pos);
            }
            break;

        case 4:
            addLast();
            break;

        case 5:
            delFirst();
            break;

        case 6:
            delLast();
            break;

        case 7:
            {
                int pos;
                printf("Enter your position: ");
                scanf("%d",&pos);
                delatPos(pos);
            }
            break;

        case 8:
            printDLL();
            break;

        case 9:
            revDLL();
            break;

        default:
            printf("Wrong choice\n");
        }
        getchar();
        printf("Do you want to continue (y/n): ");
        scanf("%c",&choice);
        }while(choice=='y' || choice=='Y');
}
