#include<stdio.h>
#include<stdbool.h>
#include<stdlib.h>
struct Demo{
    int data;
    struct Demo *next;
};

struct Demo *head=NULL;

struct Demo* createNode(){
    struct Demo *newNode=(struct Demo*)malloc(sizeof(struct Demo));
    int data;
    printf("Enter data:\n");
    scanf("%d",&newNode->data);
    newNode->next=NULL;

    return newNode;
}

void addNode(){
    struct Demo *newNode=createNode();
    if(head==NULL){
        head=newNode;
    }else{
        struct Demo *temp=head;
        while(temp->next!=NULL){
            temp=temp->next;
        }
        temp->next=newNode;
    }
}

void addFirst(){
    struct Demo *newNode=createNode();
    if(head==NULL){
        head=newNode;
    }else{
        newNode->next=head;
        head=newNode;
    }
}

void addLast(){
    addNode();
}

int count(){
    struct Demo *temp=head;
    int count=0;

    while(temp!=NULL){
        count++;
        temp=temp->next;
    }
    //printf("count=%d\n",count);
    return count;
}

int addatPos(int pos){
    int c=count();
    if(pos<=0 || pos>=c+2){
        printf("Invalid node operation\n");
        return -1;
    }else{
        if(pos==1){
            addFirst();
        }else if(pos==c+1){
            addLast();
        }else{
            struct Demo *newNode=createNode();
            struct Demo *temp=head;

            while(pos-2){
                temp=temp->next;
                pos--;
            }
            newNode->next=temp->next;
            temp->next=newNode;
        }
        return 0;
    }
}

int printLL(){
    if(head==NULL){
        printf("LL empty\n");
        return -1;
    }else{
    struct Demo *temp=head;
    while(temp!=NULL){
        printf("|%d|->",temp->data);
        temp=temp->next;
        }
        return 0;
    }
}

void delFirst(){
    struct Demo *temp=head;
    head=temp->next;
    free(temp);
}

void delLast(){
    struct Demo *temp=head;
    while(temp->next->next!=NULL){
        temp=temp->next;
    }
    free(temp->next);
    temp->next=NULL;
}

int midNode1(){
    if(head==NULL){
        printf("LL empty\n");
        return -1;
    }else{
        if(head->next==NULL){
            printf("Only one node present\n");
            return -1;
        }else{
         //   int count=count();
            int cnt=count()/2;
            struct Demo *temp=head;
            while(cnt-1){
                temp=temp->next;
                cnt--;
            }
            temp=temp->next;
            return temp->data;
        }
        return 0;
    }
}

int midNode2(){
    if(head==NULL){
        printf("LL empty\n");
    }else{
        if(head->next==NULL){
            printf("Only one node present\n");
            //return -1;
        }else{
            struct Demo *fastptr=head;
            struct Demo *slowptr=head;
            while(fastptr!=NULL && fastptr->next!=NULL){
                fastptr=fastptr->next->next;
                slowptr=slowptr->next;
            }
            return slowptr->data;
        }
}
}

int midNode3(){
    if(head==NULL){
        printf("LL empty\n");
    }else{
        if(head->next==NULL){
            printf("Only one node present\n");
            //return -1;
        }else{
            struct Demo *fastptr=head->next;
            struct Demo *slowptr=head;
            while(fastptr!=NULL && fastptr->next!=NULL){
                fastptr=fastptr->next->next;
                slowptr=slowptr->next;
            }
            return slowptr->data;
        }
}
}

int
 paliLL1(){
    int cnt=count();
    int i=0;
    int arr[cnt];
    struct Demo *temp=head;
    while(temp->next!=NULL){
        arr[i]=temp->data;
        i++;
        temp=temp->next;
    }

    int start=0,end=cnt-1;
    while(start<end){
        if(arr[start]!=arr[end]){
            return -1;
        }
        start++;
        end--;
        return 0;
    }
}

void main(){
    char choice;

    do{
        printf("1.addNode\n");
        printf("2.addFirst\n");
        printf("3.addLast\n");
        printf("4.count\n");
        printf("5.addatPos\n");
        printf("6.printLL\n");
        printf("7.deleteFirst\n");
        printf("8.deleteLast\n");
        printf("9.MidNode\n");
        printf("10.MidNode2\n");
        printf("11.MidNode3\n");
        printf("12.paliLL\n");

        int ch;
        printf("ENter your choice:\n");
        scanf("%d",&ch);

        switch(ch){
            case 1:
                addNode();
                break;

            case 2:
                addFirst();
                break;

            case 3:
                addLast();
                break;

            case 4:
                count();
                break;

            case 5:{
                int pos;
                printf("Enter the pos:\n");
                scanf("%d",&pos);

                addatPos(pos);
                }
                break;

            case 6:
                printLL();
                break;

            case 7:
                delFirst();
                break;

            case 8:
                delLast();
                break;

            case 9:
                {
                    int data=midNode1();
                    printf("Mid node is %d\n",data);
                }
                break;
            case 10:
                {
                    int data=midNode2();
                    printf("Mid node is %d\n",data);
                }
                break;
            case 11:
                {
                    int data=midNode3();
                    printf("Mid Node is %d",data);
                }
                break;

            case 12:
                {
                    int ret=paliLL1();
                    if(ret==-1){
                        printf("Not a Pallindrome\n");
                    }else{
                        printf("Pallindrome");
                    }
                }
                break;

            default:
                printf("Wrong choice\n");
                break;
        }
        getchar();
        printf("Do you want to continue:\n");
        scanf("%c",&choice);
    }while(choice=='y' || choice=='Y');
}

