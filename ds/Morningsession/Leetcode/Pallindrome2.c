//Pallindrome linked list approach 2 using midnode ,reverse inplace

#include<stdio.h>
#include<stdlib.h>
struct Demo{
    int data;
    struct Demo *next;
};

struct Demo *head=NULL;
struct Demo *head2=NULL;
int count=0;

struct Demo* createNode(){
    struct Demo *newNode=(struct Demo*)malloc(sizeof(struct Demo));
    int data;
    printf("Enter data:\n");
    scanf("%d",&newNode->data);
    newNode->next=NULL;

    return newNode;
}

void addNode(){
    struct Demo *newNode=createNode();
    if(head==NULL){
        head=newNode;
    }else{
        struct Demo *temp=head;
        while(temp->next!=NULL){
            temp=temp->next;
        }
        temp->next=newNode;
    }
}

int countnode(){
    struct Demo *temp=head;
    int count=0;

    while(temp!=NULL){
        count++;
        temp=temp->next;
    }
    //printf("count=%d\n",count);
    return count;
}

int printLL(){
    if(head==NULL){
        printf("LL empty\n");
        return -1;
    }else{
    struct Demo *temp=head;
    while(temp!=NULL){
        printf("|%d|->",temp->data);
        temp=temp->next;
        }
        return 0;
    }
}

struct Demo* midNode(){
    if(head==NULL){
        printf("LL empty\n");
       // return -1;
    }else{
        if(head->next==NULL){
            printf("Only one node present\n");
            //return -1;
        }else{
            struct Demo *fast=head;
            struct Demo *slow=head;
            while(fast!=NULL && fast->next!=NULL){
                fast=fast->next->next;
                slow=slow->next;
                count++;
            }
            return slow;
        }
    }
}

int inplacerev(){
    if(head==NULL){
        printf("LL empty\n");
        return -1;
    }else{
        if(head->next==NULL){
            printf("Only one node\n");
            return -1;
        }else{
            struct Demo *temp=midNode();
            struct Demo *head2=temp->next;
            struct Demo *temp1=NULL;
            struct Demo *temp2=NULL;

            while(head2!=NULL){
                temp2=head2->next;
                head2->next=temp1;
                temp1=head2;
                head2=temp2;
            }
            head2=temp1;
            return 0;
        }
    }
}

int palindrome(){
    struct Demo *ptr1=head;
    struct Demo *ptr2=head2;

    while(count){
        if(ptr1->data!=ptr2->data){
            return -1;
        }else{
            return 1;
        }
        count--;
        }

}


void main(){
    char choice;
    do{
        printf("1.Addnode\n");
        printf("2.midNode\n");
        printf("3.inplacerev\n");
        printf("4.palindrome\n");
        printf("5.printLL\n");
        int ch;
        printf("enter your choice\n");
        scanf("%d",&ch);

        switch(ch){
            case 1:
                addNode();
                break;

            case 2:
                {
                    struct Demo *ptr=midNode();
                    int data=ptr->data;
                    printf("Mid node is %d\n",data);
                }
                break;

            case 3:
                inplacerev();
                break;

            case 4:
                {
                    int ret=palindrome();
                    if(ret==-1){
                        printf("not a pallidrome\n");
                    }else{
                        printf("PAllindrome\n");
                    }
                }
                break;

            case 5:
                printLL();
                break;
        }
        getchar();
        printf("Do you want to continue(y/n):");
        scanf("%c",&choice);
    }while(choice == 'y' || choice == 'Y');
}
