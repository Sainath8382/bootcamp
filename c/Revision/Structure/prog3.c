//Passing structure to a function

#include <stdio.h>

struct Employee{

	int eid;
	char ename[20];
	float sal;
};

void fun1(struct Employee obj){

	printf("id = %d\n",obj.eid);
	printf("%s\n",obj.ename);
	printf("%f\n",obj.sal);
}

void fun2(struct Employee *obj){

	printf("id = %d\n",obj->eid);
	printf("%s\n",obj->ename);
	printf("%f\n",obj->sal);
}


void main(){

	struct Employee obj = {1201,"SAM",75.5};

	fun1(obj);
	fun2(&obj);
}
