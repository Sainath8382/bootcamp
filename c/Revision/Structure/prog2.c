//POinter to a structure

#include <stdio.h>
#include <string.h>

struct CricPlayer{

	char pName[10];
	float avg;
	int jerNo;
}obj = {"MS Dhoni",50.75,7};

void main(){

	struct CricPlayer *ptr = &obj;

	printf("%s\n",(*ptr).pName);
	printf("%f\n",ptr->avg);
	printf("%d\n",ptr->jerNo);
}


