//reallocate more memory to previously allocated memory
//realloc

#include<stdio.h>
#include<stdlib.h>

void main(){

	int *ptr = (int *)calloc(5,sizeof(int));
	
	for(int i=0;i<5;i++){

		*(ptr+i) = 100+i;
	}

	for(int i=0;i<5;i++)
		printf("%d\t",*(ptr+i));

	printf("\n");

	int *ptr2 = (int*)calloc(*ptr,10);

	for(int i=0;i<8;i++)
		*(ptr2+i) = 100+i;

	for(int i=0;i<8;i++)
		printf("%d\t",*(ptr2+i));

	printf("\n");
}
