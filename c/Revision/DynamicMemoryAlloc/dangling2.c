//dangling pointer scenario 2

#include <stdio.h>
#include <stdlib.h>

void main(){

	int *ptr = (int *)malloc(sizeof(int));
	*ptr = 50;

	int *ptr2 = ptr;

	free(ptr);

	printf("%d\n",*ptr2);

	printf("%d\n",*ptr);
	
}
