//continuous memory allocation
//calloc

#include<stdio.h>
#include<stdlib.h>

void main(){

	int *ptr = (int *)calloc(5,sizeof(int));
	
	for(int i=0;i<5;i++){

		*(ptr+i) = 100+i;
	}

	for(int i=0;i<5;i++)
		printf("%d\t",*(ptr+i));

	printf("\n");
}
