
//Malloc gives memory on heap

#include <stdio.h>
#include <stdlib.h>

void main(){

	int (*ptr1) = (int *)malloc(sizeof(int));

	int (*ptr2) = (int*)malloc(sizeof(int));

	*ptr1 = 100;
	*ptr2 = 200;

	printf("%d\n",*ptr1);

	printf("%d\n",*ptr2);
}

