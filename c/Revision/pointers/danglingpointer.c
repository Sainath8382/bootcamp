
#include <stdio.h>

int *iptr = 0;

void fun(){

	int x = 20;

	iptr = &x;
	printf("fun stack = %p\n", iptr);
}

void main(){

	int y = 40;

	fun();
	printf("main = %p\n",iptr);

	printf("%d\n", *iptr);		//stack frame has popped but still the adress and value of the value is preserved this 
					//is the scenario of 'Dangling Pointer'
}
