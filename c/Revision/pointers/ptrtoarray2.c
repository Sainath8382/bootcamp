
#include <stdio.h>

void main(){

	char arr1[] = {'A','B','C'};
	char arr2[] = { 'D', 'E' ,'F'};

	char (*ptr1)[3] = &arr1;

	
	char (*ptr2)[3] = &arr2;

	printf("%c\n", **ptr1);
	printf("%c\n", **ptr2);
}
