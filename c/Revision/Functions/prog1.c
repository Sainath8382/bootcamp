//Passing a function as a parameter to another function.

#include <stdio.h>

int sub(int a,int b){

	printf("%d\n",b-a);
}

void add(int x, int y){

	printf("%d\n",x+y);
}

void fun(int x, int y, void (*ptr) (int ,int) , int (*ptr2)(int,int)){

	ptr(x,y);
	ptr2(x,y);	
}

void main(){

	fun(10,20,add,sub);
}
