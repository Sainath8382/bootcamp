 
#include<stdio.h>

void callbyaddress(int *ptr){

	printf("%p\n",ptr);
	printf("%d\n",*ptr);

	*ptr = 45;
	printf("%d\n",*ptr);
}

void main(){

	int x = 10;
	
	int *ptr = &x;
	printf("%d\n",*ptr);

	callbyaddress(ptr);
	printf("%d\n",*ptr);
}


