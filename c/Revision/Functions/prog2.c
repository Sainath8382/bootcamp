
#include <stdio.h>

void add(int x,int y){

	printf("%d\n",x+y);
}

int sub(int x,int y){

	printf("%d\n",x-y);
}

void mult(int x,int y){

	printf("%d\n",x*y);
}

void fun(int x,int y,void (*ptr1)(int,int), int (*ptr2)(int,int), void (*ptr3)(int,int)){

	ptr1(x,y);
	ptr2(x,y);
	ptr3(x,y);

}

void main(){

	fun(10,20,add,sub,mult);
}

