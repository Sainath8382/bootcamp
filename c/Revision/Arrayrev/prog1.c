//Array decay code

#include <stdio.h>

void fun(char* arr){

	printf("%ld\n",sizeof(arr));	//Here pointer size is printed
}


void main(){

	char arr[3] = {'a','b','c'};

	printf("%ld\n",sizeof(arr));	//actual array size is printed

	fun(arr);
}
