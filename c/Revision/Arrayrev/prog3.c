
//Passing whole array to the function

#include <stdio.h>

void printarr1(int arr[], int arrsize){

	for(int i =0; i<arrsize ;i++)
		printf("%d\t",arr[i]);
	
	printf("\n");
}

void printarr2(int *ptr, int arrsize){

	for(int i =0; i<arrsize ;i++)
		printf("%d\t",*(ptr+i));
	
	printf("\n");
}


void main(){

	int arr[] = {10,20,30,40,50};

	int arrsize = sizeof(arr)/sizeof(arr[0]);

	printarr1(arr,arrsize);
	printarr2(arr,arrsize);
}
