 
//Function pointer

#include<stdio.h>

int addint(int x,int y){

	printf("%d\n", x+y);
}

void addfloat(float x, float y){

	printf("%f",x+y);
}

void main(){

	int x = 10,y = 20;

	float a = 10.5,b = 99.90;

	int (*ptr1)(int,int);
	ptr1 = addint;
	ptr1(x,y);

	void (*ptr2)(float,float);
	ptr2 = addfloat;
	ptr2(a,b);

}
