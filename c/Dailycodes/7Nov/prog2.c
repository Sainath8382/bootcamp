//passing structure to a function

#include<stdio.h>
struct Demo{
    int x;
    float y;
};

void fun(struct Demo obj){
    printf("%d\n",obj.x);
    printf("%f\n",obj.y);
}

void gun(struct Demo *ptr){
    printf("%d\n",ptr->x);
    printf("%f\n",(*ptr).y);
}

void main(){
    struct Demo obj={10,44.67f};

    fun(obj);   //passing structure object
    gun(&obj);  //passing structure address
}

