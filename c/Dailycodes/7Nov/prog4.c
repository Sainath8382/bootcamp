//bitfield: used to limit the value of  structure element upto a bit value

#include<stdio.h>
#pragma pack(1)

struct Demo{
    int x:2;
    int y:2;
};

void main(){
    printf("%ld\n",sizeof(struct Demo));

}

