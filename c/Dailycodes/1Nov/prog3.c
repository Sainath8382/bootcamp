//pragma pack

#include<stdio.h>
#pragma pack(1)     //pragma pack defines the actual memory of the structure without wastage

struct Demo{
    char ch1;
    int x;
    float y;
    double arr[5];
};

void main(){
    printf("%ld\n",sizeof(struct Demo));
}
