//more about structure size

#include<stdio.h>
struct Demo1{
    float f;
    char *ch;
    int x;
    int y;
};

struct Demo2{
    char ch1;
    int x;
    float y;
    char arr[10];
};

void main(){
    printf("%ld\n",sizeof(struct Demo1));
    printf("%ld\n",sizeof(struct Demo2));
}
