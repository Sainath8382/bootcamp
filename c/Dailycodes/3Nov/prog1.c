//Pointer to a structure or Structure Pointer

#include<stdio.h>
struct Movie{
    char mname[20];
    int count;
    float price;
}obj1={"Kgf",5,800.75};

void main(){
    struct Movie *sptr=&obj1;

    printf("%s\n",obj1.mname);
    printf("%d\n",obj1.count);
    printf("%f\n",obj1.price);

    printf("%s\n",(*sptr).mname);
    printf("%d\n",sptr->count);
    printf("%f\n",sptr->price);
}
