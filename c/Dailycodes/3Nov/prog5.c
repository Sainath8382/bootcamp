//nested structure
//type 2

#include<stdio.h>
#include<string.h>

struct Movie{
    char mname[20];
    struct Movieinfo{
        char actor[20];
        float imdb;
    }obj1;
};

void main(){
    struct Movie obj2={"RHTDM",{"R madhavan",8.7}};     //type2

    printf("%s\n",obj2.mname);
    printf("%s\n",obj2.obj1.actor);
    printf("%f\n",obj2.obj1.imdb);
    printf("\n");

    struct Movie obj3;
    strcpy(obj3.mname,"VV");
    strcpy(obj3.obj1.actor,"Vijay");
    obj3.obj1.imdb=7.9;

    printf("%s\n",obj3.mname);
    printf("%s\n",obj3.obj1.actor);
    printf("%f\n",obj3.obj1.imdb);
}
