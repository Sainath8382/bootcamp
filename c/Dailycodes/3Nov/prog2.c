//array of structure

#include<stdio.h>
struct cricplayer{
    char pname[20];
    int jerno;
    float rev;
};

void main(){
    struct cricplayer obj1={"KL rahul",12,75.0};
    struct cricplayer obj2={"Dravid",22,125.0};
    struct cricplayer obj3={"MSDhoni",7,150.000};
    struct cricplayer arr[]={obj1,obj2,obj3};

    for(int i=0;i<3;i++){
        printf("%s\n",arr[i].pname);
        printf("%d\n",arr[i].jerno);
        printf("%f\n",arr[i].rev);
    }
}
