//nested structure
//type1
#include<stdio.h>
#include<string.h>

struct Movieinfo{
    char actor[15];
    float imdb;
};

struct Movie{
    char mname[20];
    struct Movieinfo obj1;
};

void main(){
    struct Movie obj2;
    struct Movie obj3={"Tumbbad",{"Sohan",8.5}};

    strcpy(obj2.mname,"Vikram");
    strcpy(obj2.obj1.actor,"Kamal Hasan");
    obj2.obj1.imdb=8.9;

    printf("%s\n",obj2.mname);
    printf("%s\n",obj2.obj1.actor);
    printf("%f\n",obj2.obj1.imdb);

    printf("%s\n",obj3.mname);
    printf("%s\n",obj3.obj1.actor);
    printf("%f\n",obj3.obj1.imdb);
}
