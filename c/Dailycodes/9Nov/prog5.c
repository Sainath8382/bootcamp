//union initialisation

#include<stdio.h>
union Emp{
    int id;
    float sal;
};

void main(){
    union Emp obj1={12,45.88};  //problem here
    printf("%d\n",obj1.id);
    printf("%f\n",obj1.sal);

//proper way
    union Emp obj2;
    obj2.id=15;
    printf("%d\n",obj2.id);

    obj2.sal=14.6;
    printf("%f\n",obj2.sal);
}
