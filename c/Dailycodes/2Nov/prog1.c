#include<stdio.h>

struct Movie{
    char mname[20];
    int tickets;
    float price;
}obj1={"KGF 2",4,890.45};

void fun(){
    struct Movie obj2={"Kantara",8,1250};
    printf("%s\n",obj2.mname);
    printf("%d\n",obj2.tickets);
    printf("%f\n",obj2.price);
}

void main(){
    struct Movie obj3={"Radhe",2,450.00};
    printf("%s\n",obj3.mname);
    printf("%d\n",obj3.tickets);
    printf("%f\n",obj3.price);

    printf("%s\n",obj1.mname);
    printf("%d\n",obj1.tickets);
    printf("%f\n",obj1.price);

    fun();
}
