#include<stdio.h>
#include<stdlib.h>
#include<string.h>

struct Society{
    char sname[20];
    int wings;
    float avgrent;
};

void main(){
    struct Society *ptr=(struct Society*)malloc(sizeof(struct Society));

    strcpy((*ptr).sname,"Suncity");
    ptr->wings=8;
    ptr->avgrent=23000;

    printf("%s\n",ptr->sname);
    printf("%d\n",ptr->wings);
    printf("%f\n",ptr->avgrent);
}
