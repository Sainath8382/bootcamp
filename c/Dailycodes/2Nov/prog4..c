//object initialization using malloc

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
struct IPL{
    char sname[10];
    int Tteams;
    float prize;
};

void main(){
    struct IPL *ptr=(struct IPL*)malloc(sizeof(struct IPL));

    strcpy((*ptr).sname,"Tata");
    ptr->Tteams=8;
    (*ptr).prize=12.50;

    printf("%s\n",(*ptr).sname);
    printf("%d\n",ptr->Tteams);
    printf("%f\n",ptr->prize);
}
