#include<stdio.h>
#include<string.h>

typedef struct Employee{
    int id;
    char name[10];
    float sal;
    struct Employee *next;
}Emp;

void main(){
    Emp obj1,obj2,obj3;
    obj1.id=12;
    strcpy(obj1.name,"SAM");
    obj1.sal=15.6;
    obj1.next=&obj2;

    obj2.id=23;
    strcpy(obj2.name,"MAx");
    obj2.sal=45.00;
    obj2.next=&obj3;

    obj3.id=23;
    strcpy(obj3.name,"mary");
    obj3.sal=55.00;
    obj3.next=NULL;

    struct Employee *head=&obj1;

    printf("%d\n",head->id);
    printf("%s\n",head->name);
    printf("%f\n",head->sal);

    printf("%d\n",obj1.next->id);
    printf("%s\n",obj1.next->name);
    printf("%f\n",obj1.next->sal);

    printf("%d\n",obj2.next->id);
    printf("%s\n",obj2.next->name);
    printf("%f\n",obj2.next->sal);
}
