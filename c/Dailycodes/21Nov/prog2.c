#include<stdio.h>
#include<string.h>

struct player{
    int jerno;
    char name[10];
    float avg;
    struct player *next;
};

struct Company{
    int empcount;
    char cname[10];
    float rev;
};

void main(){
    struct player obj1,obj2,obj3;
    struct player *head=&obj1;

    head->jerno=12;
    strcpy(head->name,"Shubham");
    head->avg=45.55;
    head->next=&obj2;

    head->next->jerno=18;
    strcpy(head->next->name,"Virat");
    head->next->avg=51.9;
    head->next->next=&obj3;

    head->next->next->jerno=23;
    strcpy(head->next->next->name,"SKY");
    head->next->next->avg=71;
    head->next->next->next=NULL;

    printf("%d\n",head->jerno);
    printf("%s\n",head->name);
    printf("%f\n",head->avg);

    printf("%d\n",head->next->jerno);
    printf("%s\n",head->next->name);
    printf("%f\n",head->next->avg);

    printf("%d\n",head->next->next->jerno);
    printf("%s\n",head->next->next->name);
    printf("%f\n",head->next->next->avg);
}
