//COUNT OF ELEMENTS

#include<stdio.h>
int count(int *arr,int size){
	int count=0;
	for(int i=0;i<size-1;i++){
		if(arr[i]>arr[i+1]){
			count++;
		}
		if(arr[i]<arr[i+1]){
			count++;
		}
	}
	return count;
}

void main(){
	int size;
	printf("Enter array size: ");
	scanf("%d",&size);
	int arr[size];
	printf("Enter array elements:\n");
	for(int i=0;i<size;i++){
		scanf("%d",&arr[i]);
	}

	int ret=count(arr,size);
	printf("The count is %d\n",ret);
}
