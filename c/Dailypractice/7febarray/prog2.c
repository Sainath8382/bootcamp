//good pair
#include<stdio.h>
int goodpair(int *arr,int size,int B){
	
	for(int i=0;i<size-1;i++){
		if(arr[i]!=arr[i+1] && arr[i]+arr[i+1]==B){
			return 1;
		}
	}
	return 0;
}

void main(){
	int arr[]={1,2,3,4};
	int size=4;
	int B=8;

	int ret=goodpair(arr,size,B);
	printf("%d\n",ret);
}

