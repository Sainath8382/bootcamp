//reverse the array

#include<stdio.h>
int revarray(int *arr,int size){
	int start=0,end=size-1;
	while(start<=end){
		int temp=arr[start];
		arr[start]=arr[end];
		arr[end]=temp;
		start++;
		end--;
	}
	return arr;
}

void main(){
	int arr[]={1,2,3,4,5};
	int size=5;

	int arr2[size]=revarray(arr,size);
	for(int i=0;i<size;i++){
		printf("%d ",
