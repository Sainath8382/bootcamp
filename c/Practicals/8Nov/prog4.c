//wap to take array of n nos from user and print the array where the even numbers are printed as squares and odd numbers as cubes

#include<stdio.h>
void main(){
    int size;
    printf("ENter arr size: ");
    scanf("%d",&size);
    int arr[size];
    printf("Enter elements of array");
    for(int i=0;i<size;i++){
        scanf("%d",&arr[i]);
    }

    for(int i=0;i<size;i++){
        if(arr[i]%2==0){
            printf("%d\t",arr[i]*arr[i]);
        }else{
            printf("%d\t",arr[i]*arr[i]*arr[i]);
        }
    }
}
