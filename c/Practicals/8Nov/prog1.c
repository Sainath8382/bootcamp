//wap to program to accept string from user and print the length of the string using mystrlen()

#include<stdio.h>
int mystrlen(char* str){
    int count;
    while(*str!='\0'){
        str++;
        count++;
    }
    return count;
}

void main(){
    char str[200];
    printf("ENter string:\n");
    gets(str);

    printf("The length of the string is %d\n",mystrlen(str));
}
