//wap to take 2 strings from user and concat them and print the concated string use mystrcat

char* mystrcat(char *str1,char *str2){
    while(*str1!='\0'){
        str1++;
    }

    while(*str2!='\0'){
        *str1=*str2;
        str1++;
        str2++;
    }
    *str2='\0';
    return *str1;
}

void main(){
    char str1[20];
    char str2[20];
    printf("ENter 2 strings:\n");
    gets(str1);
    gets(str2);

    mystrcat(str1,str2);
    puts(str1);
}
