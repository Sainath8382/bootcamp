/*
WAP that accepts numbers from user seperate digit number and enter them in an array
then sort the array in ascending order
IP: 942111423
OP: 111223449
*/

#include<stdio.h>
void main(){
    int num;
    printf("Enter number: ");
    scanf("%d",&num);
    int sep,count;

    while(num!=0){
        sep=num%10;
        count++;
        num=num/10;
    }
    printf("%d\n",count);

    int arr[count];
    for(int i=0;i<count;i++){
        sep=num%10;
        arr[i]=sep;
        num=num/10;
    }

    //sorting
    for(int i=0;i<count;i++){
        int temp;
        for(int j=i;j<count;j++){
            while(arr[j]<arr[j+1]){
                temp=arr[j];
                arr[j]=arr[j+1];
                arr[j+1]=temp;
            }
        }
    }

    for(int i=0;i<count;i++){
        printf("%d |",arr[i]);
    }
}
