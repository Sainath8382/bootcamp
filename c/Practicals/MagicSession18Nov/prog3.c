//Wap that dynamically allocates a 2-D array ,take value from user and print it using malloc

#include<stdio.h>
#include<stdlib.h>
void main(){
    int rows=3;
    int cols=4;

    int *ptr=(int*)malloc(rows*cols*sizeof(int));
    printf("ENter elements: ");
    for(int i=0;i<rows;i++){
        for(int j=0;j<cols;j++){
            scanf("%d",ptr+(i*rows)+j);
        }
    }

    for(int i=0;i<rows;i++){
        for(int j=0;j<cols;j++){
            printf("%d\t",*(ptr+(i*rows)+j));
        }
        printf("\n");
    }
    free(ptr);
}
