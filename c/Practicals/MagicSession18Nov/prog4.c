//Wap that dynamically allocates a 3-D array ,take value from user and print it using malloc

#include<stdio.h>
#include<stdlib.h>
void main(){
    int plane=2,rows=3,cols=3;

    int *ptr=(int *)malloc(plane*rows*cols*sizeof(int));
    printf("ENter elements");

    for(int i=0;i<plane;i++){
        for(int j=0;j<rows;j++){
            for(int k=0;k<cols;k++){
                scanf("%d",ptr+(i*plane)+(j*rows)+k);
            }
        }
    }

    for(int i=0;i<plane;i++){
        for(int j=0;j<rows;j++){
            for(int k=0;k<cols;k++){
                printf("%d\t",*(ptr+(i*plane)+(j*rows)+k));
            }
            printf("\n");
        }
    }
}
