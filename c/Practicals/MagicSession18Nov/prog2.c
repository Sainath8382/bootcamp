//Dynamic memory allocation
//Wap that dynamically allocates a 1-D array of marks,take value from user and print it using malloc

#include<stdio.h>
#include<stdlib.h>

void main(){
    int size=5;

    int *ptr=(int *)malloc(size*sizeof(int));

    printf("Enter elements: ");
    for(int i=0;i<size;i++){
        scanf("%d",(ptr+i));
    }

    for(int i=0;i<size;i++){
        printf("%d\n",*(ptr+i));
    }
}
