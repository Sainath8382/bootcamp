//array of pointer to an array

#include<stdio.h>
void main(){
    int arr1[]={1,2,3,4};
    int arr2[]={5,6,7,8};
    int arr3[]={9,10,11,12};

    int (*ptr[3])[4]={&arr1,&arr2,&arr3};
    int (*ptr2[])[4]={&arr1,&arr2,&arr3};
}
