//strlen

#include<stdio.h>
int mystrlen(char *str){
    int count=0;

    while(*str!='\0'){
        str++;
        count++;
    }
    return count;
}

void main(){
    char str[]={'C','o','d','e'};
    int len=mystrlen(str);
    printf("%d\n",len);
}
