//strrev

#include<stdio.h>
char* strrev(char *str){
    char *temp=str;
    while(*temp!='\0'){
        temp++;
    }
    temp--;

    char x;
    while(str<temp){
        x=*str;
        *str=*temp;
        *temp=x;

        str++;
        temp--;
    }
    return str;
}

void main(){
    char str[10]={'R','a','v','i'};
    strrev(str);

    puts(str);
}
