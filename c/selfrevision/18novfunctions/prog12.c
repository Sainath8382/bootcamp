//passing array to a function

#include<stdio.h>
void ret(int *arr,int arrsize){
    for(int i=0;i<arrsize;i++){
        printf("%d\n",*(arr+i));
    }
}

void ret2(int arr[],int arrsize){
    for(int i=0;i<arrsize;i++){
        printf("%d\n",arr[i]);
    }
}

void main(){
    int arr[]={1,2,3,4,5,6};
    int arrsize=sizeof(arr)/sizeof(int);

    ret(arr,arrsize);
    ret2(arr,arrsize);

}
