//Returning multiple values from function

#include<stdio.h>
void fun(int a,int b,int *ptradd,int *ptrsub){
    *ptradd=a+b;
    *ptrsub=a-b;
}

void main(){
    int x=10;
    int y=20;

    int add;
    int sub;
    fun(10,20,&add,&sub);

    printf("%d\n",add);
    printf("%d\n",sub);
}
