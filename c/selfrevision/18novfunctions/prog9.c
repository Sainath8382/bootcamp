//passing array through a function

#include<stdio.h>
void parr(int *ptr,int arrsize){
    for(int i=0;i<arrsize;i++){
        printf("%d\t",*(ptr+i));
    }
}

void parr2(int arr[],int arrsize){
    for(int i=0;i<arrsize;i++){
        printf("%d\t",arr[i]);
    }
}

void main(){
    int arr[]={10,20,30};
    int arrsize=sizeof(arr)/sizeof(int);
    parr(arr,arrsize);
    parr2(arr,arrsize);
}
