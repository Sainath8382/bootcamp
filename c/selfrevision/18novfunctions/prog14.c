//Array of function pointers

#include<stdio.h>
void add(int x,int y){
    printf("%d\n",x+y);
}
void sub(int x,int y){
    printf("%d\n",x-y);
}
void mul(int x,int y){
    printf("%d\n",x*y);
}
void div(int x,int y){
    printf("%d\n",x/y);
}
void sq(int x,int y){
    printf("%d\t%d",x*x,y*y);
}

void main(){
    void (*ptr[5])(int,int)={add,sub,mul,div,sq};

    for(int i=0;i<5;i++){
        ptr[i](10,20);
    }
}
