//call by address

#include<stdio.h>
void fun(int *,int *);

void main(){
    int x=10;
    int y=20;

    fun(&x,&y);
    printf("%d\n",x);
    printf("%d\n",y);
}

void fun(int *x,int *y){
    int temp;
    temp=*x;
    *x=*y;
    *y=temp;

    printf("%d\n",*x);
    printf("%d\n",*y);
}
