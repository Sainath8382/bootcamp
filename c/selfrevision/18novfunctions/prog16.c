//passing a function as a parameter to another function

#include<stdio.h>
void fun(int x,int y){
    printf("%d\n",x);
    printf("%d\n",y);
}

void fun2(int x,int y,void (*ptr)(int,int)){
    ptr(10,20);
}

void main(){

    fun2(10,20,fun);
}
