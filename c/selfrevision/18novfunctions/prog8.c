//passing function to a function as a parameter

#include<stdio.h>
void mult(int a,int b){
    printf("%d\n",a*b);
}
void fun(int x,int y,void (*ptr)(int,int)){
    ptr(x,y);
}

void main(){
    fun(10,20,mult);
}
