//dangling pointer scenario 1

#include<stdio.h>
int a=10;
int *ptr=0;

void fun(){
    int x=50;
     ptr=&x;
    printf("%d\n",*ptr);
}

void main(){
    int y=20;
    printf("%d\n",y);
    fun();
    printf("%d\n",*ptr);
}
