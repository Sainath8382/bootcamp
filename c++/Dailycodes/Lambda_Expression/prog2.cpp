
#include <iostream>

int main(){

	int a = 10;
	std::string name = "main";

	auto add = [=](int x,int y) /*-> int*/{	// Here '=' means capture all and it is by value. '->' means return value if it is not given compiler 
						// understands it.

		std::cout<< a << " : " << name <<std::endl;
		return x+y;
	};

	std::cout<< add(10,20) << std::endl;

	return 0;
}

	


