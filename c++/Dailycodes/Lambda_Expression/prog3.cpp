
#include <iostream>
#include <functional>

int main(){

	int a = 10;
	std::string name = "main";

	//auto add = [=](int x,int y) -> int{	 //Replacing auto 
	
	std::function<int(int,int)> add = [=](int x,int y) -> int{	  

		std::cout<< a << " : " << name <<std::endl;
		return x+y;
	};

	std::cout<< add(10,20) << std::endl;

	return 0;
}

	


