
//Changing the variable values inside the Lambda Expression.

#include <iostream>

int main(){

	int a = 10;
	std::string name = "main";
	std::cout<< a <<std::endl;

	//auto add = [=] (int x,int y) mutable { //if this is used then there is change for a only in this lambda expressions scope only as it is call by value.
	
	auto add = [&] (auto x,auto y) mutable -> auto {	//the change in a is for the entire code as here '&' represents capture using call by reference. auto means any type data can work
		
		a++;
		std::cout<< a << " : " << name << std::endl;
		return x+y;
	};

	std::cout<< add(10,20) << std::endl;
	std::cout<< a <<std::endl;

	return 0;
}
