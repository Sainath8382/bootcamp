
//Basic Code for Lambda Expression in CPP

#include <iostream>

int main(){

	//way 1
	
	[](int x, int y){

		std::cout<< x + y <<std::endl;
	}(10,20);

	//way 2
	
	auto add = [](int x,int y){		  

		std::cout<< x+y <<std::endl;
	};

	add(10,20);

	return 0;
}
