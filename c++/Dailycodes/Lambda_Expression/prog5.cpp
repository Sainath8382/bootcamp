
// Using lambda function in which one variable is call by reference and one variable is call by value in CPP.

#include <iostream>

int main(){

	int x = 10;
	int y = 20;

	auto fun = [&x, y]() {

		x = x + 10;
		
	};
	
	fun();

	std::cout<< "After change in lamda fun x = " << x << std::endl;
	std::cout<< "No change in y = " << y << std::endl;

	return 0;
}	
