//why not to use "using namespace std;"
//	std is a namespace in which the reserved key words of cpp are present.If "using namespace std" is used then we 
//	cannot name our variables using the words such as cout and cin i.e. reserved keywords.. This code is example for the same.
//
//
#include<iostream>

//using namespace std;

int main(){
	
	int cout = 10;
//	int x=20;
//	cout<<x<<endl;
	std::cout<<cout<<std::endl;
}

//If in this code using namespace std is present then the compiler gets confused between the variable cout and the cout for printing the outputs.Thus error occurs 
//Thus using namespace std should be avoided.
