//A single function can be friend function of multiple classes at the same time.
//->Code for the same.

#include <iostream>

class Two;

class One{

	int x = 10;
	
	protected:
		
		int y = 20;
	
	public: 
		One(){
			std::cout<< "One COnstructor" <<std::endl;
		}

	private:
		void getData() const{

			std::cout<<"x =" << x <<std::endl;
			std::cout<<"y = "<< y <<std::endl;
		}
		friend void accessData(const One& obj1,const Two& obj2);
};

class Two{

	int a = 10;
	
	protected:
		
		int b =20;

	public:
		Two(){
			std::cout<< "Two Constructor" <<std::endl;
		}
		
		friend void accessData(const One& obj1,const Two& obj2);

	private:
		void getData() const{
			std::cout<<"a = " << a <<std::endl;
			std::cout<<"b = " << b <<std::endl;
		}
};

void accessData(const One& obj1,const Two& obj2){

	obj1.getData();
	obj2.getData();
}

int main(){

	One obj1;
	Two obj2;

	accessData(obj1,obj2);
}


































