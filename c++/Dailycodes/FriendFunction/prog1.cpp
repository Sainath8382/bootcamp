//Friend function : Used to access the private and protected things which are present in a class by adding a declaration of friend function into the class.
//-> Purpose of friend function is only to access the private and protected content into the class.
//-> We can also change the values of private and protected variables in the class but it not relevant as friend function should be only used to access.
//-> Thus it is seen that the friend function has parameter which is constant object.

#include <iostream>

class Demo{

	int x = 10;
	
	protected:
		int y = 20;

	public:
		Demo(){
			std::cout<< "Constructor" <<std::endl;
		}

		void getData(){
			
			std::cout<< "x = "<< x <<std::endl;
			std::cout<< "y = "<< y <<std::endl;
		}

		friend void accessData(const Demo& obj);
};

void accessData(const Demo& obj){

	std::cout<< obj.x <<std::endl;
	std::cout<< obj.y <<std::endl;

	int temp = obj.x;
//	obj.x = obj.y;		//Errors here as the object is constant and thus no changes can be done .
//	obj.y = temp;
}

int main(){

	Demo obj;

//	obj.getData();
	accessData(obj);
	obj.getData();

	return 0;
}


























		


