//Proof for class by default private in CPP

#include <iostream>

class Demo{

	int x = 10;

	public : 		//Solution for private context error
	void fun(){

		std::cout << x <<std::endl;
	}

};

int main(){

	Demo obj;
	obj.fun();

	return 0;
}
