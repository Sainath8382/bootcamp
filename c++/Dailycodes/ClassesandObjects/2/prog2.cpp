//static variables cannot be initialised in class.
//if static declared in class and accessed in the class then error.
//

#include<iostream>

class Demo{

	int x = 10;
	static int y ;

	public:
       	void fun(){

		std::cout<< y <<std::endl;
	}
};


