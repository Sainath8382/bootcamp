
#include<iostream>

class Company{

	int countEmp = 2000;
	std::string name = "IBM";

	public :

	Company(){

		std::cout<< "IN company constructor"<<std::endl;
	}

	void compInfo(){

		std::cout<< countEmp <<std::endl;
		std::cout<< name <<std::endl;
	}

};

class Employee{

	int empId = 10;
	float empsal = 98.00f;

	public: 
	Employee(){

		std::cout<< "IN employee constructor" <<std::endl;
	}

	void empInfo(){

		Company obj;

		std::cout<< empId <<std::endl;
		std::cout<< empsal <<std::endl;

//		std::cout<< obj.countEmp<< std::endl;		//private context error
//		std::cout<< obj.name <<std::endl;		//private context error

		obj.compInfo();
	}
};

int main(){
	Employee *emp = new Employee();
	emp->empInfo();

	return 0;
}
