
#include <iostream>

class Demo{

	int x = 10;
	int y = 20;

	public :

	Demo(){
		std::cout<< "IN constructor" <<std::endl;
	}

	void fun(){

		std::cout<< x << std::endl;
		std::cout<< y <<std::endl;
	}
};

int main(){

	Demo obj;
	std::cout<< sizeof(obj) <<std::endl;
	obj.fun();

	return 0;
}
