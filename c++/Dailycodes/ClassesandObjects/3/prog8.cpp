//Static variables should be declared in class to show that the variable is present in the following class.
//
//Whereas static variables should not be initialized in class as they should be present in global namespace and available to 
//all in the code.

#include<iostream>

class Demo{

	public :
		int x =10;
		//static int y=20;	//error
		static int y;

		void fun();
};

int Demo::y = 20;

void Demo::fun(){

	std::cout<< "x = "<<x<<std::endl;
	std::cout<< "y = "<<y<<std::endl;
}

int main(){

	Demo obj1;
	Demo obj2;

	obj1.fun();
	obj2.fun();

	obj1.x = 50;
	obj2.y = 100;

	obj1.fun();
	obj2.fun();

	return 0;
}

