//specifiers : private , protected , public

#include<iostream>

class Demo{

	private:
		int x =10;
	public:
		int y = 20;

	protected:
		int z = 30;
};

int main(){

	Demo obj;

	std::cout<< obj.x << obj.y << obj.z << std::endl;
}
