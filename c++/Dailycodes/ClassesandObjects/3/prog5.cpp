//Object creation Type 2

#include<iostream>

class Player{

	int jerNo = 18;
	std::string name = "Virat";

	public :

	void info(){
		
		std::cout << jerNo << " = "<<name<<std::endl;
	}
};

int main(){

	Player obj1 ;
	Player *obj2 = new Player();

	obj1.info();			//Internally : info(&obj1);
	obj2->info();			//Internally : info(obj2) *as obj2 is already an address
}


