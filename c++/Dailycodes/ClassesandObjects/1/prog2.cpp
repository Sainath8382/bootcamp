//Class in CPP :
//By default the area under class is private in Cpp.
//To access the data directly it should be declared as public:

#include<iostream>

class Player {

	//public:
	int jerNo = 18;
	std::string name = "Virat Kohli";

	public:
	void disp(){

		std::cout<< jerNo <<std::endl;
		std::cout<< name <<std::endl;
	}
};

int main(){

	Player obj;
	obj.disp();
	//std::cout<< obj.jerNo <<std::endl;
	//std::cout<< obj.name <<std::endl;
	
	return 0;
}
