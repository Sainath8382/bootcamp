
#include <iostream>

int main(){

	int y = 20;
	int x = 10;

	std::cout<< x <<std::endl;
	std::cout<< y <<std::endl;

	int const *ptr = &x;
	std::cout<< *ptr <<std::endl;

	ptr = &y;	
	*ptr = 50;		//error here as the data is constant with respect to the pointer and change is being done with help of pointer
}
