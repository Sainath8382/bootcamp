
#include <iostream>

int main(){

	int y = 20;
	int x = 10;

	std::cout<< x <<std::endl;
	std::cout<< y <<std::endl;

	//const int *ptr = &x;		//no error here as data is constant with reference to the pointer ptr.
	
//	int const *ptr = &x;		//here also same as above
	//*ptr = 40;
	
	int * const ptr = &x;		 
	std::cout << *ptr <<std::endl;

	ptr = &y ;			//error here as the pointer is constant.
	std::cout<< *ptr <<std::endl;
	return 0;
}
