//Inline function example

#include <iostream>

inline int add(int x,int y){

	return x+y;
}

int main(){

	int x = 10;
	int y = 20;
	std::cout<< add(x,y) <<std::endl;
}
