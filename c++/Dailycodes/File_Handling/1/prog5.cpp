
//Transferring data from a file to an another file.

#include <iostream>
#include <fstream>

int main(){

	std::ifstream infile("Incubator.txt");
	std::ofstream outfile("Biencaps.txt");

	std::string data;

	while(infile){

		getline(infile, data);
		outfile << data << std::endl;
	}

	infile.close();
	outfile.close();

	return 0;
}
