
//Taking user input into a file using getline function

#include <iostream>
#include <fstream>

int main(){

	std::ofstream outfile("Core2Web.txt");
	std::string inputData;

	getline(std::cin , inputData);
	outfile<< inputData;

	outfile.close();

	return 0;
}
	
