
//Reading from a file

#include <iostream>
#include <fstream>

int main(){

	std::ifstream infile("Incubator.txt");
	std::string getData;

	while(infile){

		getline(infile , getData);
		std::cout<< getData << std::endl;
	}

	infile.close();

	return 0;
}
