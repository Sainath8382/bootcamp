//copy constructor is called when one object is used to initialize another object in the code.

#include <iostream>

class Demo{

	int x =10;
	int y=20;

	public:

	Demo(){

		std::cout<< "No args constructor" <<std::endl;
	}

	Demo(int x,int y){

		this->x = x;
		this->y = y;
		std::cout<< "Para constructor" <<std::endl;
	}

	Demo (Demo &xyz){

		std::cout<< "Copy constructor" <<std::endl;
	}
};

int main(){

	Demo obj1;
	Demo obj2(1000,2000);
	Demo obj3 = obj1;
	std::cout<< &obj3 <<std::endl;
	std::cout<< &obj1 <<std::endl;

	Demo obj4;

	obj4 = obj1;
	
	return 0;
}
