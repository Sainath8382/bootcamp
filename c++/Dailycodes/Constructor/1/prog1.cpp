//There are 3 types of constructors in CPP
//default/no argument
//parameterized 
//copy constructor

//default constructor

#include <iostream>

class Demo{

	public :
		Demo(){
			std::cout<< "In no-args constructor" <<std::endl;
			std::cout<< this <<std::endl;
		}
};

int main(){

	Demo obj;
	std::cout<< &obj <<std::endl;

	return 0;
}
