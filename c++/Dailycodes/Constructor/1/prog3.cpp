//Copy constructor is by default present in class.
//Compiler adds it.
//

#include <iostream>

class Demo{
	
	public:
	
	Demo(){

		std::cout<< "In no-args constructor" <<std::endl;
	}

	Demo(int x){

		std::cout<< "In Para-constructor" <<std::endl;
		std::cout<< x <<std::endl;
	
	}

	Demo( Demo &xyz){

		std::cout<< "In copy-constructor" <<std::endl;
		std::cout<< &xyz <<std::endl;
	}
};

int main(){

	Demo obj1;
	Demo obj2(10);

	Demo obj3(obj1);
	Demo obj4 = obj1;
}
