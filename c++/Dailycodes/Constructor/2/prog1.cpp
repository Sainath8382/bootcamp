//this is used to access the instance variables in the class.

#include <iostream>

class Demo{

	public:
		int x = 10;

	public:
		Demo(){

			std::cout<< "In no-args constructor" <<std::endl;
		}

		Demo(int x){

			std::cout<< "Para" <<std::endl;
		}

		Demo( Demo &obj){

			std::cout<< "Copy" <<std::endl;
		}

		void fun(){

			std::cout<< x <<std::endl;
			std::cout<< this->x <<std::endl;
		}
};

int main(){

	Demo obj1;
	Demo obj3 = obj1;
	
	obj1.fun();
	std::cout<< obj1.x <<std::endl;
}

