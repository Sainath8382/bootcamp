//If a constructor is called from an another constructor then a temporary object is created .
//Here is the proof for the same.
//

#include <iostream>

class Demo{

	int x = 10;
	
	public:	
	Demo(){

		std::cout<< "In constructor" <<std::endl;
		std::cout<< x <<std::endl;			//In the other constructor the value of x is made 50 .
								//But here it will show the value of x as 10 . This proves that a temporary object was created
								//and called in between.
	}

	Demo(int x){

		this-> x = x;

		std::cout<< "In Para constructor" <<std::endl;
		std::cout<< x <<std::endl;
		Demo();						//From here Demo() is called.
								//thus after this stack frame is popped destructor will be called.
	}

	void getData(){

		std::cout<< x <<std::endl;
	}

	~Demo(){
		std::cout<< "IN DESTRUCTOR" <<std::endl;
	}
};

int main(){

	Demo obj(50);
	std::cout<< "END MAIN" <<std::endl;

	return 0;
}























































