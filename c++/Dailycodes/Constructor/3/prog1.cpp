//Important lecture for Interviews
//
//Copy constructor scenario 5
//*-creating array of classes and initialising the array with multiple objects then due to initialisation copy constructor is called.

#include <iostream>

class Demo{

	public:
		Demo(){

			std::cout<< "No args constructor" <<std::endl;
		}

		Demo(int x){

			std::cout<< "Para constructor" <<std::endl;
		}

		Demo( Demo &ref){

			std::cout<< "In copy" <<std::endl;
		}
};

int main(){

	Demo obj1;
	Demo obj2;
	Demo obj3;

	Demo arr[] = {obj1,obj2,obj3};

	return 0;
}
