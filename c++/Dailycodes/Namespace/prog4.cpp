
// namespace aliases : Aliases means another names 

#include <iostream>

namespace company{

	int emp = 1000;
	float rev = 200.00f;
}

namespace company{

	void compInfo(){

		std::cout<< emp << " : " << rev <<std::endl;
	}
}

int main(){

	namespace cmp = company;

	std::cout<< "Emp count = " << cmp::emp << " " << " Revenue = " << cmp::rev << std::endl;
	cmp::compInfo() ;

	return 0;
}
