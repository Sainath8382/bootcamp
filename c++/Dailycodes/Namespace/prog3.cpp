
//using two same namespaces in the same code. At runtime they both are merged together thus there is no error.

#include <iostream>

namespace company{

	int emp = 1000;
	float rev = 200.00f;
}

namespace company{

	void compInfo(){

		std::cout<< emp << " : " << rev <<std::endl;
	}
}

int main(){

	std::cout<< " Emp count = " << company::emp << " " << " Revenue = " << company::rev << std::endl;
	company::compInfo() ;

	return 0;
}
