
//Code for creating my own namespaces

#include <iostream>

namespace amazon{

	int empCount = 1000;

	void compInfo(){

		std::cout<< "Amazon emp count = " << empCount << std::endl;
	}
}

namespace nvidia{

	int empCount = 2000;

	void compInfo(){

		std::cout<< "Nvidia emp count = " << empCount << std::endl;
	}
}

int main(){

	amazon:: compInfo();
	nvidia:: compInfo();

	std::cout << amazon::empCount << std::endl; 
	std::cout << nvidia::empCount << std::endl;

	return 0;
}
