//Method overloading using normal function
//Multiplication

#include <iostream>

class Demo{

	int x = 20;

	public:

	Demo(int x){

		this->x = x;
	}

	int getData () const{

		return x;
	}

};

int operator*(const Demo& obj1, const Demo& obj2){

	return obj1.getData()*obj2.getData();
}

int main(){

	Demo obj1(10);
	Demo obj2(11);

	std::cout << obj1 * obj2 << std::endl;
}
