//Need for Operator Overloading

#include <iostream>

class Demo{

	int a = 10;
	public:
	Demo(){

	}
};

int main(){

	int x = 10;
	std::cout<< x <<std::endl;
	//function call : operator<< ( cout, x)
	//prototype:
	//ostream& operator<< (ostream&, int) -> predefined thus no error here
	
	Demo obj;
	std::cout<< obj <<std::endl;

	//function call : operator<< (cout , obj)
	//prototype:
	//ostream& operator<<(ostream&, Demo) -> this is not predefined thus error.
	
	return 0;
}
