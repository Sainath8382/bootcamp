
//second way of iterator : instead of declaring itr as a variable Directly mention it as auto in the for loop.

#include <iostream>
#include <vector>

int main() {

	std::vector v = {10,20,30,40,50};

	for(auto itr = v.begin() ; itr != v.end() ;itr++)
		std::cout << *itr << std::endl;

	return 0;
}
