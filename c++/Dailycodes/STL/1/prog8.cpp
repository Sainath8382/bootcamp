
//printing the data in reverse order in vector using iterator

#include<iostream>
#include<vector>

int main() {

	std::vector<int> v = {10,20,30,40,50};
	//std::vector<int> ::iterator itr;

	for(auto itr = v.rbegin(); itr != v.rend();itr++){	//here r means reverse

		std::cout << *itr << std::endl;
	}

	return 0;
}
