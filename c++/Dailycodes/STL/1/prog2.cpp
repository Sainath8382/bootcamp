
// sir's realtime example for vector

#include <iostream>
#include <vector>

class Player{

	int jerNo;
	std::string pName;

	public :

	Player( int jerNo , std::string pName){

		this->jerNo = jerNo;
		this->pName = pName;
	}


	void info(){

		std::cout << "Jersey :" << jerNo << "--> Player : " << pName <<std::endl;
	}

};

int main(){

	Player pOne(18,"Virat");
	Player pTwo(7, "MSD");
	Player pThree(45, "Rohit");

	std::vector<Player> pObj = {pOne, pTwo , pThree};

	for(int i=0; i< pObj.size();i++){

		pObj[i].info() ;
	}

	return 0;
}
