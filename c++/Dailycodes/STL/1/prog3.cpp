
//My realtime example for vector

#include <iostream>
#include <vector>

class Theater{

	int screenNo;
	std::string movieName;

	public:
       	Theater( int screenNo , std::string movieName){

		this->screenNo = screenNo;
		this->movieName = movieName;
	}

	void info(){

		std::cout<< "Screen no : "<< screenNo << " --> Movie " <<movieName <<std::endl;
	}

};

int main() {

	Theater sOne( 1, "Oppenheimer");
	Theater sTwo(2, "KGF");
	Theater sThree(3, "Hello");

	std::vector<Theater> Tobj = { sOne, sTwo,sThree};

	for(int i=0;i<Tobj.size();i++)
		Tobj[i].info();

	return 0;
}
