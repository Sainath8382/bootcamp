
//If the vector object is made constant and assigned to iterator variable then no change can be made to the elements .
//code for the same

#include <iostream>
#include <vector>

int main() {

	std::vector <int> v = {10,20,30,40,50};
	//std::vector<int> :: iterator itr;

	for(auto itr = v.cbegin() ; itr!=v.cend();itr++){ 	//here c means constant

		*itr += 100;
	}


	return 0;
}

