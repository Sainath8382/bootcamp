
//printing the data in reverse order in vector using iterator

#include<iostream>
#include<vector>

int main() {

	std::vector<int> v = {10,20,30,40,50};
	std::vector<int> ::iterator itr;

	for(itr = v.begin() ; itr!=v.end() ;itr++)
		std::cout<< *itr <<std::endl;

	for(auto itr1 = v.crbegin(); itr1 != v.crend();itr1++){	//here r means reverse and c means constant

		std::cout << *itr1 << std::endl;
	}

	return 0;
}
