
//vector relational operations and swap

#include<iostream>
#include<vector>

int main(){

	std::vector<int> a(5,200);
	std::vector<int> b(2,400);
	a.swap(b);

	std::vector<int> :: iterator itr;
	for(itr = a.begin(); itr != a.end(); itr++)
		std::cout<< *itr << " ";
	std::cout<< "\n";

	//Relational 
	
	std::vector<int> x = {10,20,30,40,50};
	std::vector<int> y = {50,40,30,20,10};
	
	if(x==y)
		std::cout<< "x and y equal"<<std::endl;
	if(x!=y)
		std::cout<< "x and y not equal" <<std::endl;

	return 0;
}
