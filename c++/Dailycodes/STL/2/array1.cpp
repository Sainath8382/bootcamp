
//array in stl : When this array is passed to a function then the whole array goes as a parameter.

#include <iostream>
#include <array>

void fun( std::array<int,6> arr){

	std::cout<< sizeof(arr) <<std::endl;
}

int main() {

	std::array <int,6> arr = {10,20,30,40,50,60};
	std::cout<< sizeof(arr) << std::endl;

	fun(arr);

	std::array<int,6>::iterator itr ;

	for(itr = arr.begin() ; itr!=arr.end();itr++)
		std::cout<< *itr <<std::endl;

	return 0;
}
