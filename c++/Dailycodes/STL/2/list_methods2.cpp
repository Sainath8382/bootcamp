//relational operators and swap

#include<iostream>
#include<list>

int main(){

	std::list<int> x(4,1200);
	std::list<int> y(6,1800);
	std::list<int>:: iterator itr;

	//swap : swaps the entire content of the 2 lists.
	std::swap(x,y);

	std::cout<< "LIst 1"<<std::endl;
	for(itr = x.begin(); itr!=x.end(); itr++)
		std::cout<< *itr << " ";
	std::cout<< "\n";

	for(itr = y.begin(); itr!=y.end(); itr++)
		std::cout<< *itr << " ";
	std::cout<< "\n";

	//relational operators between 2 lists.
	std::list<int> a = {10,20,30};
	std::list<int> b = {10,20,30};

	if(a==b)
		std::cout<< "Both a and b lists equal"<<std::endl;
	if(a!=b)
		std::cout<< "Not equal"<< std::endl;

	return 0;
}


	
