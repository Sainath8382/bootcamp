
//using the iterator as const and reverse for begin and end methods without using auto. i.e. By directly declaring the iterator by that type instead of declaring the iterator auto like previous codes

#include <iostream>
#include <vector>

int main() {

	std::vector<int> v = {10,20,30,40,50};
	std::vector<int> ::const_reverse_iterator itr;

	for(itr = v.crbegin(); itr!= v.crend() ;itr++) {
		//*itr = 100;
		std::cout<< *itr << std::endl;
	}

}
