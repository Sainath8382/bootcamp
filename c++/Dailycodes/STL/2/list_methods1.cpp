
//Methods : splice std::list::splice

#include <iostream>
#include <list>

int main() {

	std::list<int> lst1 = {10,20,30,40,50};
	std::list<int> lst2 = {11,21,31,41,51};

	std::list<int> :: iterator itr;
	
	itr = lst1.begin();
	itr++;
	lst1.splice(itr , lst2);

	lst2.splice(lst1.begin() , lst1, itr);

	std::cout<< " List1 contents" <<std::endl;

	for(; itr!=lst1.end() ; itr++)
		std::cout<< *itr << " ";
	std::cout<< "\n";
	
	std::cout<< " List2 contents" <<std::endl;
	itr = lst2.begin();
	for(; itr!=lst2.end() ; itr++)
		std::cout<< *itr;
	std::cout<< "\n";

	return 0; 
}
