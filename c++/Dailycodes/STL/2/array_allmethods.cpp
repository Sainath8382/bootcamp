//All the methods of the array in STL in CPP.

#include <iostream>
#include <array>

int main() {

	std::array<int,5> arr = {10,20,30,40,50};
	std::array<int,5> ::iterator itr;

	//normal iterator
	for(itr = arr.begin();itr!= arr.end();itr++){

		std::cout<< *itr << " " ;
	}
	std::cout<< "\n";

	//constant iterator : no changes can be made to the data
	std::array<int,5> ::const_iterator itr1;

	for(itr1 = arr.cbegin();itr1!= arr.cend();itr1++){

		std::cout<< *itr1 << " " ;
		}
	std::cout<< "\n";

	//reverse iterator
	std::array<int,5> ::reverse_iterator itr2;

	for(itr2 = arr.rbegin();itr2!= arr.rend();itr2++){

		std::cout<< *itr2 <<" ";
	}
	std::cout<< "\n";

	//constant_reverse iterator
	std::array<int,5> ::const_reverse_iterator itr3;

	for(itr3 = arr.crbegin();itr3!= arr.crend();itr3++){

		std::cout<< *itr3 << " ";
	}
	std::cout<< "\n";
	
	//size
	std::cout<< arr.size() <<std::endl;

	//maxsize
	std::array<int,10> arr1 = {10,20,30,40,50};
	std::cout<< arr1.max_size() << std::endl;  // output will be 10 as the elements arr1 can hold is 10 which is max size.

	//empty
	std::cout<< arr.empty() << std::endl;	//0

	//operator[]
	std::cout<< arr1[0] <<std::endl;	//10

	//at
	std::cout<< arr1.at(3) <<std::endl;	//40

	//front
	std::cout<< arr1.front() <<std::endl;	//10

	//back
	std::cout<< arr.back() << std::endl;	//50

	//data: returns pointer to the first element of the array object 
	std::cout<< arr.data() <<std::endl;	//arrays address

	//fill
	std::array<int,5> arr2;
       	arr2.fill(50);	
	for(int i=0;i<5;i++)
		std::cout<< arr2[i] << " " ;

	std::cout<< "\n";

	//swap : swaps the elements of 2 arrays having the same size and the same data in them.
	arr.swap(arr2);
	std::cout<< "First array" << std::endl;

	for(int i=0;i<5;i++)
		std::cout<< arr[i] << " " ;
	std::cout<< "\n";

	std::cout<< "SEcond array" <<std::endl;
	for(int i=0;i<5;i++)
		std::cout<< arr2[i] << " " ;

	std::cout<< "\n";	

	//std::get<>(array)
	std::cout<< std::get<2>(arr2) <<std::endl;	//30
							
	//Relational operators (array)
	std::array<int,5> array1 = {10,20,30,40,50};
	std::array<int,5> array2 = {10,20,30,40,50};
	std::array<int,5> array3 = {50,40,30,30,20};

	if(array1 == array2)
		std::cout<< "Both equal array" <<std::endl;
	if(array1 != array3)
		std::cout<< "Not  equal arrays" <<std::endl;

	return 0;
}



	


