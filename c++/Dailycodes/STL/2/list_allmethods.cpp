// All the list methods in the STL **CPP

#include<iostream>
#include<list>

int main() {

	std::list<int> lst = {10,20,30,40,50};
	
	//empty : check whether list is empty
	std::cout<< lst.empty() <<std::endl;	//0

	//size
	std::cout<< lst.size() <<std::endl;	//5

	//max_size : max elements the list can hold
	std::cout << lst.max_size() <<std::endl;	
	//std::cout << lst.max_size() <<std::endl;	

	std::cout<< lst.front() <<std::endl;
	std::cout<< lst.back() <<std::endl;

	lst.push_front(5);
	lst.push_back(55);

	std::list<int> ::iterator itr;
	for(itr = lst.begin() ; itr != lst.end() ; itr++)
		std::cout<< *itr <<" ";				//2 elementss will be added

	std::cout<< "\n";

	lst.pop_front();
	lst.pop_back();

	for(itr = lst.begin() ; itr != lst.end() ; itr++)
		std::cout<< *itr <<" ";		//the added elements will be popped
	std::cout<< "\n";

	lst.emplace_front(1);
	lst.emplace_back(100);

	lst.emplace(lst.begin(),25);
	lst.emplace(lst.end(),111);

	for(itr = lst.begin() ; itr != lst.end() ; itr++)
		std::cout<< *itr <<" ";		//the added elements will be added
	std::cout<< "\n";

	lst.resize(6);
	lst.insert(lst.begin(),70);

	for(itr = lst.begin() ; itr != lst.end() ; itr++)
		std::cout<< *itr <<" ";		//the list will be resized.

	std::cout<< "\n";

	lst.erase(lst.begin());

	for(itr = lst.begin() ; itr != lst.end() ; itr++)
		std::cout<< *itr <<" ";		//the list will be resized.

	std::cout<< "\n";

	std::list<int> lst1 = {1,2,3};
	lst1.clear();
	for(itr = lst1.begin() ; itr != lst1.end() ; itr++)
		std::cout<< *itr <<" ";		//the list will be resized.

	std::list<int> a(3,100);
	std::list<int> b(5,300);
	a.swap(b);

	for(itr = a.begin() ; itr != a.end() ; itr++)
		std::cout<< *itr <<" ";
	std::cout<< "\n";

	lst.reverse();
	for(itr = lst.begin() ; itr != lst.end() ; itr++)
		std::cout<< *itr <<" ";		//the list will be reversed.
	std::cout<< "\n";

	lst.sort();
	for(itr = lst.begin() ; itr != lst.end() ; itr++)
		std::cout<< *itr <<" ";		//the list will be sorted.

	std::cout<< "\n";
	
	std::list lst2 = {1,2,3};
	lst2.sort();		
	lst.merge(lst2);	//both lists will be merged.
	lst.unique();		//duplicate elements are removed.

	for(itr = lst.begin() ; itr != lst.end() ; itr++)
		std::cout<< *itr <<" ";		//the list will be resized.
	std::cout<< "\n";

	std::list<int> p(5,100);
      	std::list<int> q(3,300);
	p.swap(q);
	for(itr = p.begin() ; itr != p.end() ; itr++)
		std::cout<< *itr <<" ";
	
	std::cout<< "\n";

	//remove: removes specific value from the list.
	lst2.remove(2);
	for(itr = lst2.begin() ; itr != lst2.end() ; itr++)
		std::cout<< *itr <<" ";		//from the list 2 will be removed.
	std::cout<< "\n";

	return 0;
}
	
