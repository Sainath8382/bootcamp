//allocator method in vector

#include<iostream>
#include<vector>

int main() {

	std::vector<int> v ;
	int *p;
	int i;
	p = v.get_allocator().allocate(5);
	for( i=0;i<5;i++)
		v.get_allocator().construct(&p[i],i);

	std::cout<< "elements in allocated array"<<std::endl;	
	for( i=0;i<5;i++)
		std::cout<< " "<< p[i];
	std::cout<< "\n";

	for( i=0;i<5;i++)
		v.get_allocator().destroy(&p[i]);
	v.get_allocator().deallocate(p,5);

	return 0;
}
