
//remove if method 

#include <iostream>
#include<list>

bool odd(const int& no){

	return (no % 2)==1;
}

int main() {

	std::list<int> lst = {10,11,12,13,14,15};
	std::list<int> :: iterator itr;

	lst.remove_if(odd);

	std::cout<< "List contents" <<std::endl;
	for(itr = lst.begin() ; itr != lst.end() ; itr++)
		std::cout<< *itr <<std::endl;

	return 0;
}
