
//list : printing a random element of a list directly by pointer.

#include <iostream>
#include <list>
/*
std::_List_iterator<int>& operator+(std::_List_iterator<int> &lstItr, int index){

	while(index){
		lstItr++;
		index--;
	}

	return lstItr;
}
*/

int main() {

	std::list<int> lst = {10,20,30,40,50};
	std::list<int> :: iterator itr ;

	itr = lst.begin();
	
	std::cout<< *(lst.begin()) <<std::endl;
	std::cout<< *(++lst.begin()) <<std::endl;

	std::advance(itr,2) ;

	std::cout<< *itr << std::endl;

	return 0;
}
