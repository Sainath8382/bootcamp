
// Container in STL : List -> it is a Doubly Linked List . Thus when it travels then it traverses by 24 bytes in total and the iterator for list is different than iterator of vector

#include <iostream>
#include <list>

int main() {

	std::list<int> lst = {10,20,10,40,50};
	std::list<int> :: iterator itr ;

	lst.push_front(5);
	lst.push_back(60);

	for(itr = lst.begin() ; itr != lst.end(); itr++){

		std::cout<< *itr <<std::endl;
	}

	return 0;
}


