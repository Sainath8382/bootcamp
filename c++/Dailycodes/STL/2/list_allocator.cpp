
#include<iostream>
#include<list>

//list::get_allocator

int main() {

	std::list<int> lst;
	int *ptr;

	//allocate array
	ptr = lst.get_allocator().allocate(5);
	
	for(int i=0;i<5;i++)
		ptr[i] = i;	

	std::cout<< "allocated array contains"<<std::endl;

	for(int i =0; i<5;i++)
		std::cout<< ' ' << ptr[i];
	std::cout<< "\n";

	lst.get_allocator().deallocate(ptr,5);

	return 0;
}
