
//using the iterator as const and reverse for begin and end methods without using auto. i.e. By directly declaring the iterator by that type instead of declaring the iterator auto like previous codes

#include <iostream>
#include <vector>

int main() {

	std::vector<int> v = {10,20,30,40,50};
	std::vector<int> ::reverse_iterator itr;

	for(itr = v.rbegin(); itr!= v.rend() ;itr++) 
		std::cout<< *itr << std::endl;

}
