//vector all methods

#include<iostream>
#include<vector>

int main() {

	std::vector<int> v = { 10,20,30,40,50};
	std::vector<int> :: iterator itr ;

	std::cout<< "size is : "<<v.size()<<std::endl;
	v.resize(10);
	std::cout<< "Max size: "<< v.max_size() << std::endl;
	std::cout<< "capacity: "<<v.capacity()<<std::endl;	//5 allocated storage size
	std::cout<< "is empty: "<<v.empty()<<std::endl;
	v.reserve(100);
	std::cout<< "updated size : "<< v.capacity() <<std::endl;
	v.shrink_to_fit();
	std::cout<< "updated after shrink : "<< v.capacity() <<std::endl;	//5

	std::cout<< v[0] << " "<<v[4]<<" "<<std::endl;
	std::cout<< "front element: "<< v.front() <<std::endl;
	std::cout<< "back ele: "<<v.back()<<std::endl;

	int *ptr = v.data();	//returns the pointer to the vector storage location
	++ptr;
	ptr[3] = 250;
	std::cout<< "vector content : " << std::endl;
	for(itr = v.begin() ; itr != v.end() ; itr++)
		std::cout<< *itr <<std::endl;

	std::cout<< "at function op : "<< v.at(2) <<std::endl;	//returns data at specific position 

	v.push_back(111);	//add to end
	v.insert(v.end(),202);	//add to end
	v.emplace(v.begin(),250);	//add to start
	v.emplace_back(300);		//add to end
	v.pop_back();	//pop first
//	v.assign(v.begin(),125);
	v.clear();	//clears all elements

	std::cout<< "contents of vector after using clear"<<std::endl;
	for(itr = v.begin() ; itr != v.end() ; itr++)
		std::cout<< *itr <<std::endl;

	std::vector<int> v1 = {11,12,13,14};
	v1.erase(v1.begin());	//clears element at the beginning
	std::cout<< "content of new vector after erasing"<< std::endl;

	for(itr = v1.begin() ; itr != v1.end() ; itr++)
		std::cout<< *itr <<std::endl;

	std::vector<int> v2 = {21,22,23,24};
	v1.swap(v2);	//swaps 2 vectors of same size
	
	std::cout<< "Contents after swapping"<< std::endl;
	for(itr = v1.begin() ; itr != v1.end() ; itr++)
		std::cout<< *itr <<std::endl;

	std::vector<int> one;
	one.assign(4,500);
	for(itr = one.begin() ; itr != one.end() ; itr++)
		std::cout<< *itr <<" " ;
	std::cout<< "\n";
	
	std::vector<int> two;
	
	return 0;

}


