
#include <iostream>

class BelowAvgException{

	std::string exception;
	
	public:
	BelowAvgException(std::string exception){

		this->exception = exception;
	}

	std::string getException(){

		return exception;
	}
};

class AboveAvgException{

	std::string exception;

	public:
	AboveAvgException(std::string exception){

		this->exception = exception;
	}

	std::string getException(){

		return exception;
	}
};

class Employee{

	int id;
	std::string name;
	float sal;

	Employee obj1 = new Employee(1,"SAM", 4.5);
	Employee obj2 = new Employee(2,"SK", 5.5);
	Employee obj3 = new Employee(3,"MSD", 10.5);
	Employee obj4 = new Employee(4,"VK", 16.5);

	Employee arr[4] = {obj1,obj2,obj3,obj4};
	
	public:
	int Employee(int index){

		if(arr[index].sal < 5)
			throw BelowAvgException("Salary below avg");
		else if(arr[index].sal >15)
			throw AboveAvgException("Salary is very high");

		return arr[index];
	}

};

int main(){

	Employee obj ;

	try{

		for(int i=0;i<4;i++){

			std::cout<< obj[i] <<std::endl;
		}
	}catch(BelowAvgException obj){

		std::cout<< "Low salary"<< obj.getException() <<std::endl;
	}
	catch(AboveAvgException obj){

		std::cout<< "Highest salary"<< obj.getException() <<std::endl;
	}

	return 0;
}





