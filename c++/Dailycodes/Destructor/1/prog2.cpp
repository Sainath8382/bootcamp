//Difference between delete and delete[]. IMP question

#include <iostream>

class Demo{

	public:
		int *ptrArr = new int[50];
		Demo(){

			//int *ptrArr = new int[50];
			//Suppose the entire array is filled with data.

			std::cout<< "COnstructor"<<std::endl;
		}

		~Demo(){

			delete[] ptrArr;
			std::cout<< "DEstructor" <<std::endl;
		}
};

int main(){

	{
		Demo obj1;
	} return 0;
}
