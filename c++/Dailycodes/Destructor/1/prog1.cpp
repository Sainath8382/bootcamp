//Heap memory should be freed explicitly. 
//For this delete is used which is explicit way to free the heap memory. Delete calls the destructor.

#include <iostream>

class Demo{

	public: 
		Demo(){

			std::cout<< "In constructor" <<std::endl;
		}

		~Demo(){

			std::cout<< "IN Destructor" << std::endl;
		}
};

int main(){

	Demo obj;
	Demo *obj1 = new Demo();

	std::cout<< "End main" <<std::endl;
	delete obj1;	
	return 0;
}
