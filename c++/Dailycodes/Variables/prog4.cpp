//Variable shadowing 2

#include <iostream>

int x = 50;

int main(){

	int x = 10;
	std::cout<< x <<std::endl;

	{
		int x = 20;
		std::cout<< x <<std::endl;
		std::cout<< ::x <<std::endl;	//If x is not globally declared then error occurs for '::x' this error occurs due to local variable shadowing.
						//'::' i.e. scope resolution can be only used for global variables only.

		x = 30;
		std::cout<< x <<std::endl;
	}

	std::cout << x << std::endl;
}
