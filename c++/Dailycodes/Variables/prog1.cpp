//Different ways of variable initialisation

#include <iostream>

int main(){

	int x = 10;	//Value initialisation

	int y(20);	//Direct 

	std::cout << x << std::endl;
	std::cout << y << std::endl;

	int z{30};	//Uniform / Braces / List initialisation

	std::cout << z << std::endl;

	return 0;
}

