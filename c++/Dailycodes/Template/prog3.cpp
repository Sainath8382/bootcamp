
//Important code : The functions in this code are internally same as that of compiler for the template.

#include <iostream>

template<typename T>

T min(T x, T y);

template <>
int min<int>(int x ,int y){

	std::cout<< "Template" <<std::endl;
	return (x<y)? x : y;
}

int min(int x,int y){

	std::cout << "Normal" <<std::endl;
	return (x<y)? x : y;
}

template<>
float min<float> (float x ,float y){

	std::cout << "Template float"<<std::endl;
	return (x<y)? x : y;
}

float min(float x,float y){

	std::cout << "Normal float " <<std::endl;
	return (x<y)? x : y;
}

int main(){

	std::cout << min<int> (10,20) << std::endl;
	std::cout << min (10,20) << std::endl;
	std::cout << min<> (10,20) << std::endl;	//Here compiler understands the function as per the data which is being passed
							//	as parameter.
	std::cout<< min<float> (10.5f,20.5f) <<std::endl;
	std::cout<< min (10.5f,20.5f) <<std::endl;
	std::cout<< min<> (10.5f,20.5f) <<std::endl;
}

