
//Passing different datatypes as parameter to a template function.

#include <iostream>

template<typename T , typename U>

auto min(T x,U y){

/*	if(x < y)
		return x;
	else
		return y;
*/	
	return (x<y)? x :y;
}

int main(){

	std::cout << min(10,7.5) <<std::endl;
	std::cout << min('A', 100) <<std::endl;
}
