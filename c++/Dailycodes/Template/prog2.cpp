
//Template code 

#include <iostream>

template<typename T>

T min( T x, T y){

	return (x<y)? x : y;
}

int main(){

	std::cout<< min('A','B') <<std::endl;
	std::cout<< min(10,20) <<std::endl;
	std::cout<< min(20.5f, 30.5f) <<std::endl;
	std::cout<< min(20.5,30.5) <<std::endl;
}

