
//Need for inheritance
//
//--> Firstly there was composition which was used but it had many drawbacks and thus inheritance proved helpful instead of Composition.

#include <iostream>

class Employee{

	std::string ename = "Kanha";
	int empId = 255;

	public :

	Employee(){

		this -> ename = ename;
		this -> empId = empId;
	}

	~Employee(){

		std::cout<< "Employee destructor" <<std::endl;
	}

	void getInfo(){

		std::cout<< ename << " " << empId << std::endl;	
	}
};

class Company{

	std::string cname = "Veritas";
	int strEmp = 1000;
	Employee obj;

	public :
	Company(std::string cname, int strEmp){

		this->cname = cname;
		this->strEmp = strEmp;
	}

	~Company(){

		std::cout<< "Company destructor" <<std::endl;
	}

	void getInfo(){

		std::cout<< cname << " " << strEmp << std::endl;
		obj.getInfo();
	}

};

int main(){

	Company obj("Pubmatic", 1000);
	obj.getInfo();

	return 0;
}







