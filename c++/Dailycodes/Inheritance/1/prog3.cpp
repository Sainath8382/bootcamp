
#include<iostream>

class Parent{

	int x =10;

	protected:
	int y =20;

	public:
	int z = 30;

	Parent(){

	}

	Parent(int x, int y, int z){

		this->x = x;
		this->y = y;
		this->z = z;
	}

	void getData(){

		std::cout<< x << y <<z<< std::endl;
	}

};

class Child: public Parent{	//if public is not present here then the inheritance type is private Inheritance

	public:

	Child(int x, int y , int z){

		Parent(40,50,60);
	}

	void getInfo(){

		std::cout<< y << z << std::endl;
	}
};

int main(){

	Child obj(40,50,60);
	obj.getInfo();
	
	obj.getData();	//here error bcoz the getData method comes into Child as private from Parent as the inheritance type is private.

	return 0;
}
