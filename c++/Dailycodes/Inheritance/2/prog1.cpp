
#include <iostream>

class Parent{

	public:
	Parent(){

		std::cout<< "In Parent constructor" <<std::endl;
	}
};

class Child: public Parent{

	public:
		Child(){

			std::cout<< "Constructor child "<<std::endl;
		}

		void operator delete(void *ptr){
			free(ptr);
		}	
};

int main(){

	Child obj;
	Child *obj2 = new Child();

	delete obj2;
	return 0;
}
