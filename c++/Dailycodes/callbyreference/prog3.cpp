//proof that reference is internally pointer.

#include<iostream>
#pragma pack(1)
struct ref{

	int x = 10;
	int &y = x;
};

int  main(){

	ref obj;
	std::cout<< sizeof(ref) <<std::endl;	//after using pragmapack the size of structure is 12 bytes ,thus the size of &y is 8 bytes which proves that &y is a pointer.
}
