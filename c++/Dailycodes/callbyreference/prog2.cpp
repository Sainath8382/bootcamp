//when comparing x and its reference &y it is observed that the addresses of both x and &y is the same.
//it is the representation and thus the adresses are the same. Internally reference also goes as pointer.

#include <iostream>

int main(){

	int x = 10;
	int &y = x;
	int *ptr = &x;

	std::cout<< x <<std::endl;
	std::cout<< y <<std::endl;		// internally (*y)	as reference is pointer internally !
	std::cout<< ptr <<std::endl;

	std::cout<< &x <<std::endl;
	std::cout << &y <<std::endl;		// internally &(*y)
	std::cout<< &ptr<<std::endl;

	return 0;
}

