//returning the integer variable from function having int& i.e. reference as the return type.

#include<iostream>

int& fun(int x){

	int y=x;
	return y;
}

int main(){

	int a = 50;

//	fun(a);		//warning and no segmentation fault after running the code file.

	int ret = fun(a);
	std::cout<< ret << std::endl;

	return 0;
}
