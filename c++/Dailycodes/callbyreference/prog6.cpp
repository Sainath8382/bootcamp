//Call by Reference in CPP

#include<iostream>

void fun(int &ref1, int &ref2){

	int temp = ref1;
	ref1 = ref2;
	ref2 = temp;

	std::cout<< ref1 << " "<< ref2<< std::endl;
}

void fun(int x, int y){

	std::cout<< "In normal fun" <<std::endl;
}

int main(){

	int x = 10;
	int y = 20;

	std::cout<< x << " " << y <<std::endl;
	
	fun(x,y);

	std::cout<< x << " " << y <<std::endl;

	return 0;
}


